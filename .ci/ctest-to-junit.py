#!/usr/bin/env python3

"""This script converts all the seperate ctest outputs to single junit xml file"""

import os
import re
import sys
import logging
import junit_xml


def convert_ctest_junit_xml(build_dir):
    """ This function parses the ctest output file and convert it to junit xml format"""

    # Regex for parsing the ctest output
    out_tag = re.compile(r"([0-9 ]+): (.*)")
    # Regex for parsing the metadata of ctest
    status_tag = re.compile(r"([0-9\/]+) Test ([0-9#\s]+): "
                            r"([^\s]+) ([.*\s]+)([a-zA-Z\s]+)([0-9.]+) sec")

    # Initialise test_suites list
    test_suites = []
    for out_file in os.listdir(build_dir):
        # These dicts seperate the ctest output and meta data for each test
        test_output = {}
        test_metadata = {}
        # We are saving outputs with ctest prefix
        if out_file.startswith("ctest"):
            test_file = open(os.path.join(build_dir, out_file), 'r')
            lines = test_file.readlines()
            for line in lines:
                elems = out_tag.split(line)
                # Populate the test_output dict with test number as key
                if len(elems) > 1:
                    test = int(elems[1])
                    if test not in test_output:
                        test_output[test] = []
                    test_output[test].append(elems[2] + elems[3])
                elems = status_tag.split(line)
                # Populate the test_metatdat dict with test number as key
                if len(elems) > 1:
                    test = int(elems[2].strip().strip('#'))
                    test_name = elems[3].strip()
                    test_res = elems[5].strip()
                    test_time = float(elems[6])
                    test_metadata[test] = [test_name, test_res, test_time]
            # Create test cases based on result of the test. If failed add first few output lines
            test_cases = []
            for test, meta_data in test_metadata.items():
                if meta_data[1] == "Passed":
                    test_case = junit_xml.TestCase(meta_data[0], 'Imaging.IO.Test', meta_data[2])
                elif meta_data[1] == "Failed":
                    test_case = junit_xml.TestCase(meta_data[0])
                    test_case.add_error_info(output=''.join(test_output[test][4:10]))
                test_cases.append(test_case)
            # Append each test_case to the test_suite
            test_suites.append(junit_xml.TestSuite(meta_data[0].split("_")[0], test_cases))

    # Finally create unit-test.xml file as per SKA guidelines
    with open(os.path.join(build_dir, 'unit-tests.xml'), 'w') as f:
        junit_xml.TestSuite.to_file(f, test_suites, prettyprint=True)


if __name__ == '__main__':
    if len(sys.argv) < 2:
        logging.error("Usage: %s path/to/ctest_files", sys.argv[0])
        sys.exit(1)
    convert_ctest_junit_xml(sys.argv[1])
