#!/bin/bash

# This script runs gitlab runner locally. Repository files must be commited to the remote to reflect the changes. But .gitlab-ci.yml doesnt need to be committed and we can change it locally.

if [[ $# -eq 0 ]]; then
    echo 'Please provide the name of the job to run in the local pipeline'
    echo 'Usage: gitlab-local-runner.sh job_name'
    exit 0
fi

# Name of the job to run in the .gitlab-ci.yml file
JOB_NAME=$1
echo "gitlab-runner will execute $JOB_NAME job"

# Name of the container
CONTAINER_NAME=gitlab-runner

# Get all environment variables in the conf file
ENV_VARS=""
CONF_FILE=./gitlab-runner.conf
while IFS=$'\n' read line
do
    if [[ "$line" =~ \#.* || "$line" == "" ]];then
        echo "Comment line found; Ignoring..."
    else
        # echo "Found env variable: $line"
        ENV_VARS+="--env $line "
    fi
done < "$CONF_FILE"

echo "Parsed environment variables are $ENV_VARS"

# Check if docker container is already running. If not start the Container
if [[ ! -z "$(docker ps -a | grep $CONTAINER_NAME)" ]]; then
  echo "Running instance of container found"
else
  echo "No running container found. Initiating one...."
  docker run -d --name gitlab-runner \
                --restart always \
                -v $PWD:$PWD \
                -v /var/run/docker.sock:/var/run/docker.sock \
                gitlab/gitlab-runner:latest
fi

# Execute the given job in the yml file
CMD="docker exec --privileged -it -w $PWD gitlab-runner \
                                     gitlab-runner exec docker \
                                     --docker-privileged"

if [[ ! -z $ENV_VARS ]]; then
    CMD+=" $ENV_VARS"
fi

CMD+=" $JOB_NAME"

echo $CMD
eval $CMD
