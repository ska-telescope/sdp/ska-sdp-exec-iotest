
import pathlib

import numpy
import numpy.fft
import h5py
import subprocess
import sys

from ducc0.wgridder.experimental import dirty2vis

tot_image_size = 8 * 1024
config = f"{tot_image_size//1024}k[.8125]-n{tot_image_size//4096}k-1k"

facet_count = 4 # along each axis
fov = 0.0536619 * tot_image_size / 32 / 1024
facet_fov = fov / facet_count
speed_of_light = 299792458
baselines = 19306
check_iotest = False
bl_chunk = 32

# Generate configuration
proc = subprocess.Popen(
    [
        "./iotest",
        f"--rec-set={config}",
        "--facet-workers=1",
        "--producer-threads=8",
        "--vis-set=midr5",
        "--time=-870:870/12288/128",
        "--freq=0.35e9:0.4725e9/11264/128",
        "--dec=0",
        "--source-count=100",
        "--target-err=0.1",
        f"--fov={fov}",
        "--write-inputs"
    ],
    stdin = sys.stdin,
    stdout = sys.stdout
)
print(" ".join(proc.args))
proc.wait()
assert proc.returncode == 0

# Memmap facets
facets = [[ numpy.memmap(f"facet{im}_{il}.dmp", dtype=complex)
            for im in range(-(facet_count//2), (facet_count+1)//2)]
            for il in range(-(facet_count//2), (facet_count+1)//2)]
facet_size = int(numpy.sqrt(facets[0][0].shape[0]))
print("Facets size:", facet_size, "x", facet_size)

# Assemble full image
image_size = facet_size * facet_count
image_fov = facet_fov * facet_count
print("Image size: ", image_size, "x", image_size)
image = numpy.empty((image_size, image_size), dtype=complex)
for im in range(0, facet_count):
    for il in range(0, facet_count):
        image[il*facet_size:(il+1)*facet_size,
              im*facet_size:(im+1)*facet_size] = numpy.fft.fftshift(facets[il][im].reshape(facet_size, facet_size))
del facets

# Load lmn coordinates (consistency check for pixel size)
pixsize = facet_fov / facet_size
lmns = numpy.fromfile("source_lmn.dmp", dtype=float)
lmns = lmns.reshape((lmns.shape[0] // 3, 3))
xys = numpy.fromfile("source_xy.dmp", dtype=float)
xys = xys.reshape((xys.shape[0] // 2, 2))
print("Inferred pixsize: ", lmns[0][0] / xys[0][0])
print("Source count:", lmns.shape[0])
for i, lmn in enumerate(lmns):
    x,y,_ = numpy.round(lmn / pixsize)
    assert abs(x - xys[i][0]) < 1e-8
    assert abs(y - xys[i][1]) < 1e-8
    assert abs(x - lmn[0] / pixsize) < 1e-8
    assert abs(y - lmn[1] / pixsize) < 1e-8
    assert(image[int(y)+image_size//2][int(x)+image_size//2] != 0)
    image[int(y)+image_size//2][int(x)+image_size//2] = 1
assert(numpy.sum(image) == len(lmns))
print("Pixel size:", pixsize, "(both consistent with image/source coords)")

# Load UVWs
uvws = numpy.fromfile("uvw.dmp", dtype=float)
uvws = uvws.reshape((uvws.shape[0] // 3, 3))
print("UVWs: ", uvws.shape[0])

# Load frequencies
freqs = numpy.fromfile("freq.dmp", dtype=float)
print("Frequencies: ", freqs.shape[0])

# Generate UVWs corrected for DUCC
# * l/m flipped, because we seem to be using Fortran coordinate order on "image"?
# * all signs negated, because DUCC is calculating the forward Fourier transform,
#   whereas we are calculating the inverse
# * w negated, because we seemingly assume n = 1-sqrt(1-l^2-m^2)?
uvws_orig = uvws
uvws = numpy.transpose([-uvws[:,1], -uvws[:,0], uvws[:,2]])

def uvw_to_vis(freq, uvw):
    scaled = freq * uvw  / speed_of_light
    expected = 0
    for lmn in lmns:
        expected += numpy.exp(2.0j * numpy.pi * numpy.sum(scaled * lmn))
    return expected

if check_iotest:
    
    # Load HDF5 to compare against
    files = list(pathlib.Path('.').glob('out*.h5'))
    print(f"Loading {', '.join(str(f) for f in files)}...")
    vis_ref = {}
    for p in files:
        f = h5py.File(p, 'r')
        nant = f['vis']['uvw'].shape[0]
        ntime = f['vis']['uvw'].shape[1]
        nbl = nant * (nant-1) // 2
        assert uvws.shape[0] == ntime * nbl
        for a1 in f['vis']:
            if a1 in ['frequency', 'time', 'uvw']:
                continue
            for a2 in f['vis'][a1]:
                if int(a1) not in vis_ref:
                    vis_ref[int(a1)] = {int(a2): f['vis'][a1][a2]['vis']}
                elif int(a2) not in vis_ref[int(a1)]:
                    vis_ref[int(a1)][int(a2)] = f['vis'][a1][a2]['vis']
                else:
                    vis_ref[int(a1)][int(a2)] = \
                            numpy.array(vis_ref[int(a1)][int(a2)]) + \
                            numpy.array(f['vis'][a1][a2]['vis'])
    nant = max(int(k) for k in vis_ref.keys())+2
    bls = sorted(sum(([ (a1, a2) for a2 in  vis] for a1,vis in vis_ref.items()), []),
                 key=lambda a1_a2: (a1_a2[1], a1_a2[0]))
    nbl_check = len(bls)
    print("Antennas:", nant)
    assert nbl == nbl_check
    print("Baselines:", nbl, " (consistent with UVW shape & all data complete)")

    nfreq = freqs.shape[0]
    h5_uvw = h5py.File(files[0],'r')['vis']['uvw']
 
    for time_bl, uvw_time in enumerate(uvws_orig):
        bl = time_bl // ntime
        time = time_bl % ntime
        a1, a2 = bls[bl]
        assert numpy.all(numpy.isclose(uvw_time, h5_uvw[a2][time] - h5_uvw[a1][time])), \
            f"{time_bl} {bl} ({a1}/{a2}) {time} {h5_uvw[a2][time] - h5_uvw[a1][time]}"
        diffs = []
        for ifreq, freq in enumerate(freqs):
            vis = vis_ref[a1][a2][time][ifreq] * tot_image_size**2
            expected = uvw_to_vis(freq, uvw_time)
            diffs.append(numpy.abs(vis - expected))
        print(numpy.sqrt(numpy.mean(numpy.array(diffs)**2)))
        
       
    exit(0)

assert uvws.shape[0] % baselines == 0
time_steps = uvws.shape[0] // baselines
print("Baselines: ", baselines);
print("Time steps: ", time_steps);

del uvws_orig
uvws_bl = uvws.reshape(baselines, time_steps, 3)
w_order = numpy.argsort(-numpy.abs(uvws_bl[:,time_steps//2,2]))

for bl in range(0, baselines, bl_chunk):

    bls = w_order[bl:bl+bl_chunk]
    uvw = uvws_bl[bls]
    uvw = numpy.array(uvw).reshape(len(bls) * time_steps, 3)
    vis_count = freqs.shape[0] * uvw.shape[0]
    print(f"Degridding {vis_count} visibilities ({16*vis_count/1e9} GB)")
    print(f" w-range: {numpy.min(numpy.abs(uvw[:,2]))}-{numpy.max(numpy.abs(uvw[:,2]))}")
    
    vis = dirty2vis(
        uvw=uvw,
        freq=freqs,
        dirty=image.real,
        pixsize_x=pixsize,
        pixsize_y=pixsize,
        epsilon=1e-4,
        verbosity=1,
        do_wgridding=True,
        divide_by_n=False,
        flip_v=False,
        nthreads=32)

    assert(vis.shape == (uvw.shape[0], freqs.shape[0]))
    del vis
