#!/bin/bash

#SBATCH --job-name=iotestTrial
#SBATCH --account=interns202321
#SBATCH --partition=work
#SBATCH --output=/scratch/interns202321/prachidavay/results/%j.log
#SBATCH --exclusive

#GENERATED FILE

set -e

#Load spack/0.19.0, fftw/3.3.10, hdf5/1.12.2-api-v112
module load spack/0.19.0 fftw/3.3.10 hdf5/1.12.2-api-v112
echo " "

#Initialise variables
NUM_NODES=$SLURM_JOB_NUM_NODES
NUM_PROCS=$SLURM_NTASKS
NUM_CPUS=$SLURM_CPUS_PER_TASK

#Name for benchmark 
BENCH_NAME=$NUM_NODES-nodes-$NUM_PROCS-processes-$NUM_CPUS-cpus$1$2-date:`date +%d.%m.%Y-%H:%M`
echo "$BENCH_NAME"
echo " "

#Directory for executable
WORK_DIR=/scratch/interns202321/prachidavay/ska-sdp-exec-iotest/build

#Change to executable diirectory
cd $WORK_DIR

#Check if correct parameters are provided 
if [ "$#" -lt 1 ]; then
	echo "error: please enter sufficient command line args (at least 1; image size)"
	exit 1

#If correct parameters, then execute code
elif [ "$#" -gt 1 ]; then

	echo "Job ID: $SLURM_JOB_ID"
	echo "Job start time: `date`"
	echo "Running on master node: `hostname`"
	echo " "

	#mkdir /scratch/interns202321/prachidavay/results/visibilities
	CMD="srun ./iotest $1 $2 $3 $4 $5 $6" #/scratch/interns202321/prachidavay/results/visibilities/out%d.h5"
	echo $CMD
	echo "Number of nodes: $NUM_NODES, number of processes: $NUM_PROCS, number of cpus: $NUM_CPUS"
	echo " "
	
	eval $CMD
fi

echo "Job finish time: `date`"

#Save all results in appropriate directory 
mkdir /scratch/interns202321/prachidavay/results/$BENCH_NAME
mv /scratch/interns202321/prachidavay/results/$SLURM_JOB_ID.log /scratch/interns202321/prachidavay/results/$BENCH_NAME/$SLURM_JOB_ID.log
#mv /scratch/interns202321/prachidavay/results/visibilities /scratch/interns202321/prachidavay/results/$BENCH_NAME/visibilities
