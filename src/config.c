#include "grid.h"
#include "config.h"

#include <hdf5.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <errno.h>
#include <pthread.h>
#include <fcntl.h>

const int WORK_REQUEST_TAG = 0;

// Split work on most expensive subgrid into at least this many
// baseline groups per subgrid worker
const int WORK_MIN_GRANULARITY = 4;

// How many "cheap" w-planes per "expensive" w-plane?
const int W_PLANE_RATIO = 8;

double get_time_ns()
{
    struct timespec ts;
    clock_gettime(CLOCK_REALTIME, &ts);
    return ts.tv_sec + (double)ts.tv_nsec / 1000000000;
}

void bl_bounding_box(struct bl_data *bl_data, bool negate,
                     int tstep0, int tstep1,
                     int fstep0, int fstep1,
                     double *uvw_l_min, double *uvw_l_max)
{

    // Check time start and end (TODO - that's simplifying quite a bit)
    double uvw0[3], uvw1[3];
    int i;
    for (i = 0; i < 3; i++) {
        uvw0[i] = bl_data->uvw_m_2[tstep0*3+i] - bl_data->uvw_m_1[tstep0*3+i];
        uvw1[i] = bl_data->uvw_m_2[tstep1*3+i] - bl_data->uvw_m_1[tstep1*3+i];
    }

    // Conversion factor to uvw in lambda
    double scale0 = uvw_m_to_l(1, bl_data->freq[fstep0]),
           scale1 = uvw_m_to_l(1, bl_data->freq[fstep1]);
    if (negate) { scale0 = -scale0; scale1 = -scale1; }

    // Determine bounding box
    for (i = 0; i < 3; i++) {
        uvw_l_min[i] = fmin(fmin(uvw0[i]*scale0, uvw0[i]*scale1),
                            fmin(uvw1[i]*scale0, uvw1[i]*scale1));
        uvw_l_max[i] = fmax(fmax(uvw0[i]*scale0, uvw0[i]*scale1),
                            fmax(uvw1[i]*scale0, uvw1[i]*scale1));
    }
}

void bl_bounding_subgrids(struct bl_data *bl_data, bool negate,
                          double lam_sg, double wstep_sg, int a1, int a2,
                          int *sg_min, int *sg_max)
{
    double uvw_l_min[3], uvw_l_max[3];
    bl_bounding_box(bl_data, negate,
                    0, bl_data->time_count-1,
                    0, bl_data->freq_count-1,
                    uvw_l_min, uvw_l_max);

    // Convert into subgrid indices
    sg_min[0] = (int)round(uvw_l_min[0]/lam_sg);
    sg_min[1] = (int)round(uvw_l_min[1]/lam_sg);
    sg_min[2] = (int)round(uvw_l_min[2]/wstep_sg);
    sg_max[0] = (int)round(uvw_l_max[0]/lam_sg);
    sg_max[1] = (int)round(uvw_l_max[1]/lam_sg);
    sg_max[2] = (int)round(uvw_l_max[2]/wstep_sg);

    /* printf("BL %d/%d u %g-%g (%d-%d) v %g-%g (%d-%d) w %g-%g (%d-%d)\n", */
    /*        a1,a2, */
    /*        uvw_l_min[0], uvw_l_max[0], sg_min[0], sg_max[0], */
    /*        uvw_l_min[1], uvw_l_max[1], sg_min[1], sg_max[1], */
    /*        uvw_l_min[2], uvw_l_max[2], sg_min[2], sg_max[2]); */
}

struct worker_prio {
    int worker;
    int nbl;
};

static int compare_prio_nbl(const void *_w1, const void *_w2)
{
    const struct worker_prio *w1 = (const struct worker_prio *)_w1;
    const struct worker_prio *w2 = (const struct worker_prio *)_w2;
    return w1->nbl > w2->nbl;
}

static int bin_baseline(struct vis_spec *spec, struct bl_data *bl_data,
                        double lam_sg, double wstep_sg,
                        int nsubgrid, int nwlevels,
                        int a1, int a2, int iu, int iv, int iw,
                        double *pmin_w, double *pmax_w)
{
    assert (iu >= 0 && iu < nsubgrid);
    assert (iv >= 0 && iv < nsubgrid);
    int chunks = 0, tchunk, fchunk;

    double sg_min_u = lam_sg*(iu-nsubgrid/2) - lam_sg/2;
    double sg_min_v = lam_sg*(iv-nsubgrid/2) - lam_sg/2;
    double sg_min_w = wstep_sg*(iw-nwlevels/2) - wstep_sg/2;
    double sg_max_u = lam_sg*(iu-nsubgrid/2) + lam_sg/2;
    double sg_max_v = lam_sg*(iv-nsubgrid/2) + lam_sg/2;
    double sg_max_w = wstep_sg*(iw-nwlevels/2) + wstep_sg/2;
    int ntchunk = spec_time_chunks(spec);
    int nfchunk = spec_freq_chunks(spec);
    double min_w = sg_max_w, max_w = sg_min_w;

    // Count number of overlapping chunks
    for (tchunk = 0; tchunk < ntchunk; tchunk++) {

        // Check whether time chunk fall into positive u. We use this
        // for deciding whether coordinates are going to get flipped
        // for the entire chunk. This is assuming that a chunk is
        // never big enough that we would overlap an extra subgrid
        // into the negative direction.
        int tstep_mid = tchunk * spec->time_chunk + spec->time_chunk / 2;
        bool positive_u = bl_data->uvw_m_2[tstep_mid * 3] - bl_data->uvw_m_1[tstep_mid * 3] >= 0;

        // Check frequencies. We adjust step length exponentially so
        // we can jump over non-matching space quicker, see
        // below. This bit of code is likely a bit too smart for its
        // own good!
        int fstep = 1;
        for (fchunk = 0; fchunk < nfchunk; fchunk+=fstep) {

            // Determine chunk bounding box
            double uvw_l_min[3], uvw_l_max[3];
            bl_bounding_box(bl_data, !positive_u,
                            tchunk * spec->time_chunk,
                            fmin(spec->time_count, (tchunk+1) * spec->time_chunk) - 1,
                            fchunk * spec->freq_chunk,
                            fmin(spec->freq_count, (fchunk+fstep) * spec->freq_chunk) - 1,
                            uvw_l_min, uvw_l_max);

            if (uvw_l_min[0] < sg_max_u && uvw_l_max[0] > sg_min_u &&
                uvw_l_min[1] < sg_max_v && uvw_l_max[1] > sg_min_v &&
                uvw_l_min[2] < sg_max_w && uvw_l_max[2] > sg_min_w) {

                if (fstep == 1) {
                    // Found a chunk
                    chunks++;
                    min_w = fmin(min_w, uvw_l_min[2]);
                    max_w = fmax(max_w, uvw_l_max[2]);
                } else {
                    // Went too fast. Decrease step length, recheck.
                    fstep /= 2;
                    fchunk -= fstep;
                }
            } else {
                // Speed up. Increase step length.
                fchunk -= fstep;
                fstep *= 2;
            }
        }
    }

    // Return bounds
    if (pmin_w) *pmin_w = fmax(min_w, sg_min_w);
    if (pmax_w) *pmax_w = fmin(max_w, sg_max_w);
    return chunks;
}

static int min(int a, int b) { return a > b ? b : a; }
static int max(int a, int b) { return a < b ? b : a; }


// Collect work for a specific subgrid cube
static void collect_work(struct vis_spec *spec, struct bl_data *bl_data,
                         double lam_sg, double wstep_sg,
                         int nsubgrid, int nwlevels,
                         int *sg_mins, int *sg_maxs,
                         int iu, int iv, int iw,
                         int *nchunks, struct subgrid_work_bl **work)
{

    int a1, a2, bl=0;
    for (a1 = 0; a1 < spec->cfg->ant_count; a1++) {
        for (a2 = a1+1; a2 < spec->cfg->ant_count; a2++, bl++) {
            int *sg_min = sg_mins + bl * 3, *sg_max = sg_maxs + bl * 3;

            // Bounds check
            if (!(iv >= nsubgrid/2+sg_min[1] && iv <= nsubgrid/2+sg_max[1] &&
                  iu >= nsubgrid/2+sg_min[0] && iu <= nsubgrid/2+sg_max[0] &&
                  iw >= nwlevels/2+sg_min[2] && iw <= nwlevels/2+sg_max[2]) &&
                !(iv >= nsubgrid/2-sg_max[1] && iv <= nsubgrid/2-sg_min[1] &&
                  iu >= nsubgrid/2-sg_max[0] && iu <= nsubgrid/2-sg_min[0] &&
                  iw >= nwlevels/2-sg_max[2] && iw <= nwlevels/2-sg_min[2]))
                continue;

            // Count actual number of chunks
            double min_w, max_w; // lowest/highest w-level touched by a chunk
            struct bl_data *bl_data_bl = bl_data + spec->cfg->ant_count*a2+a1;
            int chunks = bin_baseline(spec, bl_data_bl,
                                      lam_sg, wstep_sg,
                                      nsubgrid, nwlevels,
                                      a1, a2, iu, iv, iw, &min_w, &max_w);
            assert(min_w >= wstep_sg * (iw-nwlevels/2) - wstep_sg/2);
            assert(max_w <= wstep_sg * (iw-nwlevels/2) + wstep_sg/2);
            if (!chunks)
                continue;

            // Sum up
            *nchunks+=chunks;

            // Make sure we don't add a baseline twice
            assert(!*work || (*work)->a1 != a1 || (*work)->a2 != a2);

            // Find baseline insert position (sort by w so that we
            // maximise locality and minimise redundant w-tower FFTs
            // down the line)
            struct subgrid_work_bl *pos = *work, *prev = NULL;
            while (pos && pos->min_w < min_w) {
                prev = pos; pos = pos->next;
            }

            // Add work structure
            struct subgrid_work_bl *wbl = (struct subgrid_work_bl *)
                malloc(sizeof(struct subgrid_work_bl));
            wbl->a1 = a1; wbl->a2 = a2; wbl->chunks=chunks;
            wbl->next = pos;
            wbl->min_w = min_w;
            wbl->max_w = max_w;
            wbl->bl_data = bl_data_bl;
            if (prev)
                prev->next = wbl;
            else
                *work = wbl;
            fflush(stdout);


        }
    }

}

// Log only on the first rank
static void config_log(const char *s, ...)
{

    // Get rank, skip if not first
    int my_rank = 0;
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    if (my_rank) return;

    // Otherwise print as normal
    va_list va;
    va_start(va, s);
    vprintf(s, va);
    va_end(va);
}

// Bin baselines per overlapping subgrid
static void collect_baselines(struct vis_spec *spec, struct bl_data *bl_data,
                              double lam, double lam_sg, double wstep_sg,
                              bool dump_baselines,
                              int **pnchunks, struct subgrid_work_bl ***pbls,
                              int *pnsubgrid, int *pnwlevels)
{

    // Determine baseline bounding boxes
    int nbl_total = spec->cfg->ant_count * (spec->cfg->ant_count - 1) / 2;
    int *sg_mins = (int *)malloc(sizeof(int) * 3 * nbl_total),
        *sg_maxs = (int *)malloc(sizeof(int) * 3 * nbl_total);
    int a1, a2, bl = 0;
    int max_sg_u = 0, max_sg_v = 0, max_sg_w = 0;
    const int nant = spec->cfg->ant_count;
    double start = get_time_ns();
    for (a1 = 0; a1 < nant; a1++) {
        for (a2 = a1+1; a2 < nant; a2++, bl++) {
            int *mins = sg_mins + bl * 3,
                *maxs = sg_maxs + bl * 3;
            bl_bounding_subgrids(bl_data + a1 + nant*a2, false,
                                 lam_sg, wstep_sg, a1, a2,
                                 mins, maxs);
            max_sg_u = max(max_sg_u, max(-mins[0], maxs[0]));
            max_sg_v = max(max_sg_v, max(-mins[1], maxs[1]));
            max_sg_w = max(max_sg_w, max(-mins[2], maxs[2]));
        }
    }

    // Determine number of subgrid bins we need
    int nsubgrid = 2 * (int)ceil(1. / 2 / (lam_sg / lam)) + 3;
    int nwlevels = 2 * max_sg_w + 1;
    nsubgrid = min(nsubgrid, max(2*max_sg_u+1, 2*max_sg_v+1));
    config_log("Binning chunks (%dx%dx%d subgrids)...\n",
               nsubgrid, nsubgrid, nwlevels);

    // Allocate chunk statistics
    int *nchunks = (int *)calloc(sizeof(int), nsubgrid * nsubgrid * nwlevels);
    struct subgrid_work_bl **bls = (struct subgrid_work_bl **)
        calloc(sizeof(struct subgrid_work_bl *), nsubgrid * nsubgrid * nwlevels);
    int iv, iu, iw;
    #pragma omp parallel for collapse(3) schedule(dynamic,8)
    for (iv = 0; iv < nsubgrid; iv++) {
        for (iu = 0; iu < nsubgrid; iu++) {
            for (iw = 0; iw < nwlevels; iw++) {
                int ix = iw * nsubgrid*nsubgrid + iv * nsubgrid + iu;
                collect_work(spec, bl_data,
                             lam_sg, wstep_sg, nsubgrid, nwlevels,
                             sg_mins, sg_maxs, iu, iv, iw,
                             nchunks + ix, bls + ix);
            }
        }
    }

    free(sg_mins); free(sg_maxs);
    config_log(" %g s\n", get_time_ns() - start);

    // Produce dump if requested
    if (dump_baselines) {
        config_log("Baseline bins:\n---\niu,iv,iw,chunks\n");
        for (iv = 0; iv < nsubgrid; iv++) {
            for (iu = nsubgrid/2; iu < nsubgrid; iu++) {
                int chunks = 0;
                for (iw = 0; iw < nwlevels; iw++) {
                    struct subgrid_work_bl *bl;
                    for (bl = bls[nsubgrid*nsubgrid*iw+nsubgrid*iv+iu];
                         bl; bl=bl->next) {
                        chunks += bl->chunks;
                        //config_log("%g ", bl->min_w);
                    }
                }
                if (chunks) {
                    config_log("%d,%d,%d\n", iu, iv, chunks);
                }
            }
        }
        config_log("---\n");
    }
    fflush(stdout);

    *pnchunks = nchunks;
    *pbls = bls;
    *pnsubgrid = nsubgrid;
    *pnwlevels = nwlevels;
}

// Pop given number of baselines from the start of the linked list
static struct subgrid_work_bl *pop_chunks(struct subgrid_work_bl **bls, int n, int *nchunks)
{
    struct subgrid_work_bl *first = *bls;
    struct subgrid_work_bl *bl = *bls;
    *nchunks = 0;
    assert(n >= 1);
    if (!bl) return bl;
    while (n > bl->chunks && bl->next) {
      *nchunks += bl->chunks;
      n-=bl->chunks;
      bl = bl->next;
    }
    *nchunks += bl->chunks;
    *bls = bl->next;
    bl->next = NULL;
    return first;
}

struct subgrid_work *find_subgrid_work(struct work_config *cfg, int worker, int work)
{
    // Find n-th work packet for given subgrid worker
    int i;
    for (i = 0; i < cfg->subgrid_work_assigned; i++)
        if (cfg->subgrid_work[i].worker == worker)
            if (!work--)
                return cfg->subgrid_work + i;
    return NULL;
}

static bool generate_subgrid_work_assignment(struct work_config *cfg)
{
    struct vis_spec *spec = &cfg->spec;
    if (cfg->subgrid_workers == 0) {
        fprintf(stderr, "WARNING: Visibility mode, but no subgrid workers. Nothing to do!\n");
        return true;
    }

    config_log("Covering %d time steps, %d channels, %d baselines\n",
               cfg->spec.time_count, cfg->spec.freq_count,
               cfg->spec.cfg->ant_count * (cfg->spec.cfg->ant_count - 1) / 2);

    // Count visibilities per sub-grid
    int *nbl; struct subgrid_work_bl **bls;
    const double lam = config_lambda(cfg);
    int nsubgrid, nwlevels;
    collect_baselines(spec, cfg->bl_data, lam,
                      cfg->sg_step / cfg->theta,
                      cfg->sg_step_w * cfg->wstep,
                      cfg->config_dump_baseline_bins,
                      &nbl, &bls, &nsubgrid, &nwlevels);

    // Count how many sub-grids actually have visibilities
    int npop = 0, nbl_total = 0, nbl_max = 0;
    int *nbl_w = alloca(sizeof(int) * nwlevels);
    memset(nbl_w, 0, sizeof(int) * nwlevels);
    int iu, iv, iw;
    for (iw = 0; iw < nwlevels; iw++) {
        for (iv = 0; iv < nsubgrid; iv++) {
            for (iu = 0; iu < nsubgrid; iu++) {
                int n = nbl[iw * nsubgrid*nsubgrid + iv * nsubgrid + iu];
                if (n) npop++;
                nbl_total += n;
                nbl_w[iw] += n;
                nbl_max = max(n, nbl_max);
            }
        }
    }

    double coverage = (double)npop * cfg->sg_step * cfg->sg_step / lam / lam;

    // We don't want bins that are too full compared to the average -
    // determine at what point we're going to split them.
    int work_max_nbl = (nbl_max + cfg->subgrid_workers - 1) / cfg->subgrid_workers / 8;
    config_log("%d subgrid baseline bins (%.3g%% coverage), %.5g average chunks per subgrid, "
               "splitting above %d\n",
               npop, coverage*100, (double)nbl_total / npop, work_max_nbl);

    // Now count again how much work we have total, and per
    // column. Note that we ignore grid data at u < 0, as transferring
    // half the grid is enough to reconstruct a real-valued image.
    int nwork = 0, max_work_column = 0;
    for (iw = 0; iw < nwlevels; iw++) {
        for (iu = 0; iu < nsubgrid; iu++) {
            int nwork_start = nwork;
            for (iv = 0; iv < nsubgrid; iv++) {
                int nv = nbl[iw * nsubgrid*nsubgrid + iv * nsubgrid + iu];
                nwork += (nv + work_max_nbl - 1) / work_max_nbl;
            }
            // How much work in this column?
            if (nwork - nwork_start > max_work_column)
                max_work_column = nwork-nwork_start;
        }
    }

    // Determine w-plane order
    cfg->w_plane_count = nwlevels;
    cfg->w_plane_order = (int *)calloc(sizeof(int), nwlevels);
    int next_cheap_w_plane = -nwlevels / 2; // edges are cheap
    int next_expensive_w_plane = 0; // centre is expensive
    int cheap = 0; // start with the most expensive one
    for (iw = 0; iw < nwlevels - 1; iw++) {
        if (cheap > 0) {
            cheap--;
            cfg->w_plane_order[iw] = next_cheap_w_plane;
            // Progress outer -> inner, alternating sides
            config_log("cheap %d\n", next_cheap_w_plane);
            next_cheap_w_plane = -next_cheap_w_plane;
            if (next_cheap_w_plane < 0)
                next_cheap_w_plane++;
        } else {
            cheap = W_PLANE_RATIO - 1;
            cfg->w_plane_order[iw] = next_expensive_w_plane;
            config_log("expensive %d\n", next_expensive_w_plane);
            // Progress inner -> outer, alternating sides
            next_expensive_w_plane = -next_expensive_w_plane;
            if (next_expensive_w_plane >= 0)
                next_expensive_w_plane++;
        }
    }
    assert(next_expensive_w_plane == next_cheap_w_plane);
    cfg->w_plane_order[nwlevels-1] = next_expensive_w_plane;

    // Worker priority order for acquiring new work
    struct worker_prio *worker_prio = alloca(sizeof(worker_prio) * cfg->subgrid_workers);
    int i;
    for (i = 0; i < cfg->subgrid_workers; i++) {
        worker_prio[i].worker = i;
        worker_prio[i].nbl = 0;
    }

    // Allocate work description
    cfg->subgrid_work = (struct subgrid_work *)calloc(sizeof(struct subgrid_work), nwork);

    // Go through columns and assign work
    int iworker = 0, iwork = 0, iwork_assigned = 0, tag = 0;
    double w_coverage = 0;
    cfg->iu_min = INT_MAX; cfg->iu_max = INT_MIN;
    cfg->iv_min = INT_MAX; cfg->iv_max = INT_MIN;
    for (iw = 0; iw < nwlevels; iw++) {
      int iwlevel = cfg->w_plane_order[iw] + nwlevels/2;
      for (iu = 0; iu < nsubgrid; iu++) {

        // Generate column of work
        int start_bl;
        for (iv = 0; iv < nsubgrid; iv++) {
            int ix = iwlevel * nsubgrid*nsubgrid + iv * nsubgrid + iu;
            for (start_bl = 0; start_bl < nbl[ix]; start_bl += work_max_nbl) {

                // Assign work to next worker
                struct subgrid_work *work = cfg->subgrid_work + iwork;

                work->iu = iu - nsubgrid/2;
                work->iv = iv - nsubgrid/2;
                work->iw = iwlevel - nwlevels/2;
                work->subgrid_off_u = cfg->sg_step * work->iu;
                work->subgrid_off_v = cfg->sg_step * work->iv;
                work->subgrid_off_w = cfg->sg_step_w * work->iw;
                work->bls = pop_chunks(&bls[ix], work_max_nbl, &work->nbl);
                if (!work->nbl)
                    break;
                work->tag = tag;

                if (work->iu < cfg->iu_min) cfg->iu_min = work->iu;
                if (work->iu > cfg->iu_max) cfg->iu_max = work->iu;
                if (work->iv < cfg->iv_min) cfg->iv_min = work->iv;
                if (work->iv > cfg->iv_max) cfg->iv_max = work->iv;

                // Assign worker
                if (iwork < cfg->subgrid_workers * cfg->vis_subgrid_queue_length ||
                    cfg->subgrid_workers <= 1) {
                    work->worker = iworker;
                    iwork_assigned++;
                } else {
                    work->worker = -1;
                }
                work->assign_tag = -1;
                work->assign_request = MPI_REQUEST_NULL;

                // Save back how many chunks were assigned
                worker_prio[iworker].nbl += work->nbl;

                // Keep track of how much space we cover along the w-axis
                double w_min = 1e15, w_max = -1e15;
                for (struct subgrid_work_bl *bl = work->bls; bl; bl = bl->next) {
                    if (bl->min_w < w_min) w_min = bl->min_w;
                    if (bl->max_w > w_max) w_max = bl->max_w;
                }
                w_coverage += (w_max - w_min) / cfg->wstep + cfg->w_gridder.size;

                // Next chunk!
                iworker++; iworker %= cfg->subgrid_workers; iwork++;
                tag += cfg->facet_max_work;
            }
        }
      }
    }
    free(bls); free(nbl);

    // Log result of work assignment
    cfg->subgrid_work_count = iwork;
    cfg->subgrid_work_assigned = iwork_assigned;
    int unassigned_work = iwork - iwork_assigned;
    config_log("%d work items (%d per worker), %d pre-assigned (limit %d), %d dynamic\n",
               iwork, iwork / cfg->subgrid_workers,
               iwork_assigned, cfg->subgrid_workers * cfg->vis_subgrid_queue_length, unassigned_work);

    // Determine average
    int64_t sum = 0;
    for (i = 0; i < cfg->subgrid_workers; i++) {
        sum += worker_prio[i].nbl;
    }
    int average = sum / cfg->subgrid_workers;

    // Swap statically assigned work to even out profile
    bool improvement; int nswaps = 0;
    do {
        improvement = false;

        // Sort worker priority
        qsort(worker_prio, cfg->subgrid_workers, sizeof(void *), compare_prio_nbl);

        // Walk through worker pairs
        int prio1 = 0, prio2 = cfg->subgrid_workers - 1;
        while(prio1 < prio2) {
            int diff = worker_prio[prio2].nbl - worker_prio[prio1].nbl;
            int worker1 = worker_prio[prio1].worker;
            int worker2 = worker_prio[prio2].worker;

            // Find a work item to switch
            int iwork, best_diff = diff;
            struct subgrid_work *best_w1 = NULL, *best_w2 = NULL;
            for (iwork = 0; ; iwork++) {
                struct subgrid_work *w1 = find_subgrid_work(cfg, worker1, iwork);
                struct subgrid_work *w2 = find_subgrid_work(cfg, worker2, iwork);
                if (!w1 || !w2) break;
                int wdiff = w2->nbl - w1->nbl;
                if (abs(diff - 2*wdiff) < best_diff) {
                    best_w1 = w1; best_w2 = w2; best_diff = abs(diff - 2*wdiff);
                }
            }

            // Found a swap?
            if (best_w1) {

                struct subgrid_work w = *best_w1;
                *best_w1 = *best_w2;
                *best_w2 = w;

                worker_prio[prio1].nbl += best_w1->nbl - best_w2->nbl;
                worker_prio[prio2].nbl += best_w2->nbl - best_w1->nbl;

                improvement = true;
                nswaps++;
                break;
            }

            // Step workers. Keep the one that is further away from the
            // average.
            if (abs(worker_prio[prio2].nbl - average) >
                abs(worker_prio[prio1].nbl - average)) {
                prio1++;
            } else {
                prio2--;
            }
        }

    } while(improvement);

    // Statistics
    int min_vis = INT_MAX, max_vis = 0;
    for (i = 0; i < cfg->subgrid_workers; i++) {
        int j; int vis = 0;
        for (j = 0; ; j++) {
            struct subgrid_work *work = find_subgrid_work(cfg, i, j);
            if (!work) break;
            vis += work->nbl;
            //config_log("%d ", work->nbl);
        }
        //config_log(" -> %d %d\n", vis, worker_prio[i].nbl);
        min_vis = fmin(vis, min_vis);
        max_vis = fmax(vis, max_vis);
    }
    config_log("Assigned workers %d chunks min, %d chunks max (after %d swaps)\n", min_vis, max_vis, nswaps);

    if (cfg->config_dump_subgrid_work) {
        config_log("Subgrid work (after swaps):\n---\nworker,work,chunks,iu,iv,iw\n");
        for (i = 0; i < cfg->subgrid_workers; i++) {
            int j;
            for (j = 0; ; j++) {
                struct subgrid_work *work = find_subgrid_work(cfg, i, j);
                if (!work) break;
                if (work->nbl > 0) {
                    config_log("%d,%d,%d,%d,%d,%d\n", i,j, work->nbl,
                               work->iu, work->iv, work->iw);
                }
            }
        }
        config_log("---");
    }

    return true;
}

static bool generate_facet_work_assignment(struct work_config *cfg)
{

    // This is straightforward: We just assume that all facets within
    // the field of view are set. Note that theta is generally larger
    // than the FoV, so this won't cover the entire image.
    double yB = (double)cfg->recombine.yB_size / cfg->recombine.image_size;
    int nfacet = ceil(cfg->spec.fov / cfg->theta / yB - 1e-15);

    // An even number of facets requires facet offsets that are half
    // a facet size (only for an odd number of facets the offset for the
    // middle facet would be 0)
    if (nfacet % 2 == 0 && (cfg->recombine.yB_size / cfg->recombine.facet_spacing) % 2 != 0) {
        // We can suggest an alternate configuration that would make
        // having an even number of facets possible - it involves
        // correcting the subgrid spacing up, which can reduce the
        // amount of usable subgrid space. However, this is often more
        // efficient than working with a whole extra row/column of
        // facets... Now, note that *normally*, our input
        // configurations should have already been chosen with this
        // trade-off in mind, so we're mainly trying to be helpful
        // here in case people override the FoV size manually...
        int sg_s = cfg->recombine.subgrid_spacing * 2;
        config_log("WARNING: Forcing facet count to be odd due to facet spacing restriction!\n"
                   "         Might want to try --recombine=%d,%d,%d,%d,%d,%d,%d,%d\n",
                   cfg->recombine.image_size, sg_s,
                   cfg->recombine.yB_size, cfg->recombine.yN_size, cfg->recombine.yP_size,
                   sg_s * (cfg->recombine.xA_size / sg_s),
                   cfg->recombine.xM_size, cfg->recombine.xMxN_yP_size);
        nfacet++;
    }

    config_log("%dx%d facets covering %g FoV (%g deg, facet %g, grid theta %g)\n",
               nfacet, nfacet, cfg->spec.fov, 2 * asin(cfg->spec.fov/2) / M_PI * 180,
               cfg->theta * yB, cfg->theta);
    if (nfacet * yB > 1) {
        // This is fine for recombination, but w-towers precision
        // really plumments in this scenario.
        config_log("WARNING: facets overlap edge of image, expect low w precision!\n");
    }
    if (cfg->facet_workers == 0) return true;

    // Allocate work array
    cfg->facet_max_work = (nfacet * nfacet + cfg->facet_workers - 1) / cfg->facet_workers;
    cfg->facet_count = nfacet * nfacet;
    cfg->facet_work = (struct facet_work *)
        calloc(sizeof(struct facet_work), cfg->facet_workers * cfg->facet_max_work);

    int i;
    for (i = 0; i < nfacet * nfacet; i++) {
        int iworker = i % cfg->facet_workers, iwork = i / cfg->facet_workers;
        struct facet_work *work = cfg->facet_work + cfg->facet_max_work * iworker + iwork;
        work->il = (i / nfacet) - nfacet/2;
        work->im = (i % nfacet) - nfacet/2;
        work->facet_off_l = work->il * cfg->recombine.yB_size;
        work->facet_off_m = work->im * cfg->recombine.yB_size;
        if (nfacet % 2 == 0) {
            work->facet_off_l += cfg->recombine.yB_size / 2;
            work->facet_off_m += cfg->recombine.yB_size / 2;
        }
        work->set = true;
    }

    return true;
}

/**
 * \brief No visibilities involved, so generate work assignment where we
 * simply redistribute all data from a number of facets matching
 * the number of facet workers.
 * \return True if the operation succeeds in generating the work assignment, false otherwise.
 */
static bool generate_full_redistribute_assignment(struct work_config *cfg)
{
    assert(!cfg->spec.time_count);

    if (cfg->facet_workers > 0) {
        int nfacet = cfg->recombine.image_size / cfg->recombine.yB_size;
        cfg->facet_max_work = (nfacet * nfacet + cfg->facet_workers - 1) / cfg->facet_workers;
        cfg->facet_count = nfacet * nfacet;
        cfg->facet_work = (struct facet_work *)
            calloc(sizeof(struct facet_work), cfg->facet_max_work * cfg->facet_workers);
        int i;
        for (i = 0; i < nfacet * nfacet; i++) {
            int iworker = i % cfg->facet_workers, iwork = i / cfg->facet_workers;
            struct facet_work *work = cfg->facet_work + cfg->facet_max_work * iworker + iwork;
            work->il = i / nfacet;
            work->im = i % nfacet;
            work->facet_off_l = work->il * cfg->recombine.yB_size;
            work->facet_off_m = work->im * cfg->recombine.yB_size;
            work->set = true;
        }
    }

    if (cfg->subgrid_workers > 0) {
        int nsubgrid = cfg->recombine.image_size / cfg->sg_step;
        cfg->subgrid_work_count = nsubgrid * nsubgrid;
        cfg->subgrid_work_assigned = nsubgrid * nsubgrid;
        cfg->subgrid_work = (struct subgrid_work *)
            calloc(sizeof(struct subgrid_work), cfg->subgrid_work_count);
        int i;
        for (i = 0; i < cfg->subgrid_work_count; i++) {
            struct subgrid_work *work = cfg->subgrid_work + i;
            work->iu = i / nsubgrid;
            work->iv = i % nsubgrid;
            work->iw = 0;
            work->subgrid_off_u = work->iu * cfg->sg_step;
            work->subgrid_off_v = work->iv * cfg->sg_step;
            work->subgrid_off_w = work->iw * cfg->sg_step_w;
            work->nbl = 1;
            work->worker = i % cfg->subgrid_workers;
            work->tag = i * cfg->facet_max_work;
            work->assign_tag = -1;
            work->assign_request = MPI_REQUEST_NULL;
            // Dummy 0-0 baseline
            work->bls = (struct subgrid_work_bl *)calloc(sizeof(struct  subgrid_work_bl), 1);
        }
        cfg->iu_min = cfg->iv_min = 0;
        cfg->iu_max = cfg->iv_max = nsubgrid-1;
    }

    cfg->w_plane_count = 1;
    cfg->w_plane_order = (int *)malloc(sizeof(int));
    cfg->w_plane_order[0] = 0;

    return true;
}

/**
 * \brief Initialise \ref work_config.
 */
void config_init(struct work_config *cfg)
{
    memset(cfg, 0, sizeof(*cfg));
    cfg->gridder.x0 = 0.5;
    cfg->w_gridder.x0 = 0.5;
    cfg->shear_u = cfg->shear_v = 0;
    cfg->config_dump_baseline_bins = false;
    cfg->config_dump_subgrid_work = false;
    cfg->produce_parallel_cols = false;
    cfg->produce_retain_bf = true;
    cfg->produce_batch_rows = 16;
    cfg->produce_queue_length = 4;
    cfg->produce_threads = 0;
    cfg->vis_bls_per_task = 256;
    cfg->vis_subgrid_queue_length = 256;
    cfg->vis_task_queue_length = 96;
    cfg->vis_chunk_queue_length = 4096;
    cfg->vis_writer_count = 2;
    cfg->vis_fork_writer = false;
    cfg->vis_check_existing = false;
    cfg->vis_checks = 16384;
    cfg->grid_checks = 1365; // Use non-power-of-2
    cfg->vis_check_error = 1;
    cfg->vis_max_error = 1;
    cfg->vis_round_to_wplane = false;
    cfg->vis_extra_grid_stride = 16;
    cfg->vis_interleave_wplanes = false;

    cfg->statsd_socket = -1;
    cfg->statsd_rate = 1;
}

static void print_power2(int x)
{
    if (x >= 1024 * 1024 && x % (1024 * 1024) == 0) {
        config_log("%dM", x / 1024 / 1024);
    } else if (x >= 1024 && x % 1024 == 0) {
        config_log("%dk", x / 1024);
    } else {
        config_log("%d", x);
    }
}

/**
 * \brief Set the recombination configuration.
 * \return True if the recombination configuration is successfully initialised, false otherwise.
 */
bool config_set(struct work_config *cfg,
                int image_size, int subgrid_spacing,
                char *pswf_file, double W,
                int yB_size, int yN_size, int yP_size,
                int xA_size, int xM_size, int xMxN_yP_size)
{
    // Set recombination configuration
    config_log("\nInitialising streaming FT (image size "); print_power2(image_size);
    config_log(", facet FFT "); print_power2(yP_size);
    config_log(", subgrid FFT "); print_power2(xM_size);
    if (W != 0)
        config_log(", pswf W=%g)...\n", W);
    else
        config_log(", pswf file %s)...\n", pswf_file);

    if (!recombine2d_set_config(&cfg->recombine, image_size, subgrid_spacing, pswf_file, W,
                                yB_size, yN_size, yP_size,
                                xA_size, xM_size, xMxN_yP_size))
        return false;
    cfg->sg_step = cfg->recombine.xA_size;

    // Check error
    config_log(" done, expected accuracy: %g\n", estimate_error(image_size, yN_size, yB_size,
                                                                xA_size, xM_size, cfg->recombine.pswf));

    return true;
}

void config_free(struct work_config *cfg)
{
    free(cfg->vis_path); cfg->vis_path = NULL;
    free(cfg->facet_work); cfg->facet_work = NULL;
    free(cfg->gridder.data); cfg->gridder.data = NULL;
    free(cfg->gridder.corr); cfg->gridder.corr = NULL;
    free(cfg->w_gridder.data); cfg->w_gridder.data = NULL;
    free(cfg->w_gridder.corr); cfg->w_gridder.corr = NULL;

    int i;
    for (i = 0; i < cfg->subgrid_work_count; i++) {
        while (cfg->subgrid_work[i].bls) {
            struct subgrid_work_bl *bl = cfg->subgrid_work[i].bls;
            cfg->subgrid_work[i].bls = cfg->subgrid_work[i].bls->next;
            free(bl);
        }
    }
    free(cfg->subgrid_work); cfg->subgrid_work = NULL;
    free(cfg->w_plane_order); cfg->w_plane_order = NULL;
    if (cfg->spec.cfg)
        free(cfg->spec.cfg->xyz);
    free(cfg->spec.cfg); cfg->spec.cfg = NULL;
    free(cfg->spec.ha_sin); cfg->spec.ha_sin = NULL;
    free(cfg->spec.ha_cos); cfg->spec.ha_cos = NULL;
    free(cfg->bl_data); cfg->bl_data = NULL;

    free(cfg->source_xy); cfg->source_xy = NULL;
    free(cfg->source_lmn); cfg->source_lmn = NULL;
    free(cfg->source_corr); cfg->source_corr = NULL;

    recombine2d_free(&cfg->recombine);

    if (cfg->statsd_socket != -1) close(cfg->statsd_socket);
    cfg->statsd_socket = -1;
}

/**
 * \brief Convert \ref vis_spec data to \ref bl_data.
 * \brief Make baseline specifications. Right now this is the same for every
 * baseline, but this will change for baseline dependent averaging.
 *
 * If shear_u/shear_v are passed, baseline coordinates are
 * automatically sheared to minimise w. Note that this also shears m/l
 * depending on n, effectively distorting the sky sphere.
 *
 * \return Pointer to converted \ref bl_data.
 */
struct bl_data *vis_spec_to_bl_data(struct vis_spec *spec, double *shear_u, double *shear_v) {
    int i;

    const int nant = spec->cfg->ant_count;
    const int ntime = spec->time_count;
    const int nfreq = spec->freq_count;
    const int bldata_size = sizeof(struct bl_data) * nant * nant;
    const int data_size = sizeof(double) * (ntime * (1 + nant * 3) + nfreq);

    // Allocate all required memory in one block (makes it easier to free)
    struct bl_data *bls = (struct bl_data *)malloc(bldata_size + data_size);
    double *time = (double *)(bls + nant*nant);
    double *freq = time + ntime;
    double *uvws = freq + nfreq;

    // Generate values
    for (i = 0; i < ntime; i++) {
        time[i] = spec->time_start + spec->time_step * i;
    }
    for (i = 0; i < nfreq; i++) {
        freq[i] = spec->freq_start + spec->freq_step * i;
    }
    int ant;
    for (ant = 0; ant < nant; ant++) {
        for (i = 0; i < ntime; i++) {
            ha_to_uvw_sc_1(spec->cfg, ant,
                           spec->ha_sin[i], spec->ha_cos[i],
                           spec->dec_sin, spec->dec_cos,
                           uvws + 3*(ant*ntime + i));
        }
    }

    // Shear?
    if (shear_u) {
        assert(shear_v);

        // Determine optimum for linear regression
        //
        //  shear_u * u + shear_v * v + error = w

        double sum_uu=0, sum_uv=0, sum_uw=0, sum_vv=0, sum_vw=0;
        double min_w = 1e51, max_w = -1e50;
        int a1, a2;
        for (a1 = 0; a1 < nant; a1++) {
            for (a2 = 0; a2 < a1; a2++) {
                for (i = 0; i < ntime; i++) {
                    int i1 = 3*(a1*ntime + i);
                    int i2 = 3*(a2*ntime + i);
                    double u = uvws[i1+0] - uvws[i2+0];
                    double v = uvws[i1+1] - uvws[i2+1];
                    double w = uvws[i1+2] - uvws[i2+2];
                    if (w < min_w) min_w = w;
                    if (w > max_w) max_w = w;
                    sum_uu += u * u; sum_uv += u * v; sum_uw += u * w;
                    sum_vv += v * v; sum_vw += v * w;
                    if (fabs(w) > max_w) max_w = fabs(w);
                }
            }
        }

        double denom = sum_uu * sum_vv - sum_uv * sum_uv;
        double su = (sum_vv * sum_uw - sum_uv * sum_vw) / denom;
        double sv = (sum_uu * sum_vw - sum_uv * sum_uw) / denom;

        // Now apply shear transformation to uvw coordinates
        for (ant = 0; ant < nant; ant++) {
            for (i = 0; i < ntime; i++) {
                uvws[3*(ant*ntime + i) + 2] -=
                    su * (uvws[3*(ant*ntime + i) + 0]) +
                    sv * (uvws[3*(ant*ntime + i) + 1]);
            }
        }
        double max_w2 = 0;
        for (a1 = 0; a1 < nant; a1++) {
            for (a2 = 0; a2 < a1; a2++) {
                for (i = 0; i < ntime; i+=ntime-1) {
                    int i1 = 3*(a1*ntime + i);
                    int i2 = 3*(a2*ntime + i);
                    double w = uvws[i1+2] - uvws[i2+2];
                    if (fabs(w) > max_w2) max_w2 = fabs(w);
                }
            }
        }
        *shear_u = su;
        *shear_v = sv;
        config_log("Shear factors u: %g, v: %g (max_w: %g -> %g)\n", *shear_u, *shear_v, max_w, max_w2);

    }

    // Create baseline structures
    int a1, a2;
    for (a1 = 0; a1 < nant; a1++) {
        for (a2 = 0; a2 < nant; a2++) {
            struct bl_data *bl = bls + a1 + nant*a2;
            bl->antenna1 = a1;
            bl->antenna2 = a2;
            bl->time_count = spec->time_count;
            bl->time = time;
            bl->freq_count = spec->freq_count;
            bl->freq = freq;
            bl->uvw_m_1 = uvws + 3*a1*ntime;
            bl->uvw_m_2 = uvws + 3*a2*ntime;
        }
    }

    return bls;
}

/**
 * \brief Determine sky position in (l,m,n) coordinate system
 * corresponding to given image pixel coordinate, taking sky sphere
 * distortion due to u/v shears into account.
 */
void config_pixel_to_lmn(int il, int im, double lam, double su, double sv,
                         double *pl, double *pm, double *pn)
{

    double lp = il / lam;
    double mp = im / lam;

    // The shear modified "w' = w - shear_u * u - sv * w", which
    // means that we get the same result through the Fourier transform
    // as long as we reproject:
    //
    //  l u + m v + n w = l' u + m' v + n w'
    //                  = (l' - su * n) + (m' - sv * n) + n w
    //   => l' = l + su (sqrt(1 - l^2 - m^2) - 1)
    //      m' = m + sv (sqrt(1 - l^2 - m^2) - 1)
    //
    // We know l'/m' here, i.e. the position in the reprojected
    // image. What we are after is the "n" of the original sky
    // direction that was projected here. Solving the above for l/m
    // using Mathematica yields the following mouthfuls:
    //
    // l = (lp + lp sv^2 + su - mp su sv - su c) / (1 + su^2 + sv^2)
    // m = (mp + mp su^2 + sv - lp su sv - sv c) / (1 + su^2 + sv^2)
    // where c = sqrt(1 - 2 lp su - 2 mp sv + 2 lp su mp sv - lp^2 (1 + sv^2) - mp^2 (1 + su^2) )
    double c = sqrt(1 - 2*su*lp - 2*sv*mp + 2*su*lp*sv*mp - lp*lp*(1+sv*sv) - mp*mp*(1+su*su) );
    double l = (lp*(1 + sv*sv) + su*(1 - mp*sv - c)) / (1 + su*su + sv*sv);
    double m = (mp*(1 + su*su) + sv*(1 - lp*su - c)) / (1 + su*su + sv*sv);
    double n = sqrt(1 - l*l - m*m) - 1;

    // Check
    assert(fabs(l + su * n - lp) < 1e-10);
    assert(fabs(m + sv * n - mp) < 1e-10);

    // From which we can now calculate n. As n' = n, this is also the
    // sought-after depth of the sky sphere at (l', m')
    if (pl) *pl = lp;
    if (pm) *pm = mp;
    if (pn) *pn = n;
}

/**
 * \brief Configure and set \ref bl_data from the provided \ref vis_spec.
 */
void config_set_visibilities(struct work_config *cfg,
                             struct vis_spec *spec,
                             const char *vis_path,
                             int sg_step_w, int margin,
                             double target_err)
{
    const int image_size = cfg->recombine.image_size;
    assert(image_size != 0); // Must be set previously

    // Copy
    cfg->spec = *spec;
    if (vis_path)
        cfg->vis_path = strdup(vis_path);

    // Cache cosinus + sinus values
    cfg->spec.ha_sin = (double *)malloc(sizeof(double) * cfg->spec.time_count);
    cfg->spec.ha_cos = (double *)malloc(sizeof(double) * cfg->spec.time_count);
    int it;
    for (it = 0; it < cfg->spec.time_count; it++) {
        double t = spec->time_start + spec->time_step * it;
        cfg->spec.ha_sin[it] = sin(t * M_PI / 12);
        cfg->spec.ha_cos[it] = cos(t * M_PI / 12);
    }
    cfg->spec.dec_sin = sin(cfg->spec.dec);
    cfg->spec.dec_cos = cos(cfg->spec.dec);

    // Calculate baseline data (UVWs)
    // This also determines the shear factors
    if (!cfg->write_inputs) {
        cfg->bl_data = vis_spec_to_bl_data(&cfg->spec, &cfg->shear_u, &cfg->shear_v);
    } else {
        // We assume that we are writing this for the purpose of
        // feeding it to another gridder, which likely won't support
        // shear.
        printf("WARNING: Disabling shear as we are dumping inputs!\n");
        cfg->bl_data = vis_spec_to_bl_data(&cfg->spec, NULL, NULL);

        // Dump UVW data
        const int ntime = cfg->spec.time_count;
        const int nant = cfg->spec.cfg->ant_count;
        const int nbl = nant * (nant-1) / 2;
	const size_t uvw_size = sizeof(double) * nbl * 3 * ntime;
        double *d_uvw_m = (double *)malloc(uvw_size);
        int a1, a2, ibl = 0;
        for (a1 = 0; a1 < nant; a1++) {
            for (a2 = 0; a2 < a1; a2++, ibl++) {
                struct bl_data *bl = cfg->bl_data + a1 + nant*a2;
                int i;
                for (i = 0; i < ntime*3; i++) {
                    d_uvw_m[ibl*ntime*3+i] = bl->uvw_m_1[i] - bl->uvw_m_2[i];
                }
            }
        }
	printf("Dumping uvw (%lu bytes)...\n", uvw_size);
        write_dump(d_uvw_m, uvw_size, "uvw.dmp");
        free(d_uvw_m);

        // Dump frequencies
        write_dump(cfg->bl_data->freq,
                   sizeof(*cfg->bl_data->freq) * cfg->spec.freq_count,
                   "freq.dmp");
    }

    // Calculate grid step lengths
    double max_lm = spec->fov / 2;
    cfg->theta = max_lm / cfg->gridder.x0;

    // Calculate maximum n at edges
    int max_il = image_size * cfg->gridder.x0;
    double l, m, n;
    double max_n = 0;
    config_pixel_to_lmn(max_il, max_il, image_size / cfg->theta, cfg->shear_u, cfg->shear_v, &l, &m, &n);
    if (-n > max_n) max_n = -n;
    config_pixel_to_lmn(max_il, -max_il, image_size / cfg->theta, cfg->shear_u, cfg->shear_v, &l, &m, &n);
    if (-n > max_n) max_n = -n;
    config_pixel_to_lmn(-max_il, max_il, image_size / cfg->theta, cfg->shear_u, cfg->shear_v, &l, &m, &n);
    if (-n > max_n) max_n = -n;
    config_pixel_to_lmn(-max_il, -max_il, image_size / cfg->theta, cfg->shear_u, cfg->shear_v, &l, &m, &n);
    if (-n > max_n) max_n = -n;
    cfg->wstep = cfg->w_gridder.x0 / max_n;
    config_log("Grid resolution u/v: %g lambda (max_lm=%g), w: %g lambda (max_n=%g)\n",
               1/cfg->theta, max_lm, cfg->wstep, max_n);
    if (max_n > 0.25) {
        config_log("WARNING: Large field of view. This will be *extremely* inefficient.\n");
    }

    // Calculate uvw cube split
    if (margin % cfg->recombine.subgrid_spacing != 0) {
        margin = (margin + cfg->recombine.subgrid_spacing - 1) / cfg->recombine.subgrid_spacing;
        margin *= cfg->recombine.subgrid_spacing;
        config_log("WARNING: Margin increased to %d (subgrid spacing %d)",
                   margin, cfg->recombine.subgrid_spacing);
    }
    cfg->sg_step = cfg->recombine.xA_size - margin;

    // If target error is not given, we gun for an order magnitude off
    // the best error we would get from the recombination
    // configuration. Note that this is overkill in most cases, and
    // leads to overly defensive (occassionally even impossible!)
    // configurations. The better way to do this would be to estimate
    // the target error from the (w-)gridder, which is generally the
    // true accuracy bottleneck...
    if (target_err == 0) {
        target_err = estimate_error(cfg->recombine.image_size,
                                    cfg->recombine.yN_size, cfg->recombine.yB_size,
                                    cfg->recombine.xA_size, cfg->recombine.xM_size,
                                    cfg->recombine.pswf);

        // Put at least *some* space between the target and the theoretical maximum.
        // This is entirely arbitrary. Warn about it.
        target_err *= 10;
        config_log("WARNING: Estimating target error %g from recombination configuration.\n"
                   "         This is generally overly defensive, try passing --target-err!\n",
                   target_err);
    }

    // Determine wstep (i.e. distance of w-tower planes). Can be given
    // on the command line, or derived automatically by observing the
    // error at a certain distance from the subgrid centre
    int xA_size_eff = cfg->sg_step - cfg->gridder.size;
    int sg_step_est = estimate_max_w_planes(cfg->recombine.image_size,
                                            cfg->recombine.yN_size, cfg->recombine.yB_size,
                                            xA_size_eff, cfg->recombine.xM_size,
                                            cfg->recombine.pswf,
                                            cfg->wstep, cfg->theta, spec->fov, target_err, true);
    sg_step_est -= cfg->w_gridder.size;
    cfg->sg_step_w = (sg_step_w ? sg_step_w : sg_step_est);

    // Subgrid step must be even
    if (cfg->sg_step_w % 2 != 0) {
        cfg->sg_step_w--;
    }

    int margin_eff = cfg->recombine.xM_size - xA_size_eff;
    if (sg_step_w) {
        config_log("w-stacking step: %d (%g lambda) - estimated %d (%g lambda, for margin %d/%g%%, err %g)\n",
                   cfg->sg_step_w, cfg->sg_step_w*cfg->wstep, sg_step_est, sg_step_est*cfg->wstep,
                   margin_eff, 100. * margin_eff / cfg->recombine.xM_size, target_err);
    } else {
        config_log("w-stacking step: %d (%g lambda, estimated for margin %d [%.01f%%], err %g)\n",
                   cfg->sg_step_w, cfg->sg_step_w*cfg->wstep,
                   margin_eff, 100. * margin_eff / cfg->recombine.xM_size, target_err);
    }
    if (cfg->sg_step_w < 0) {
        fprintf(stderr, "ERROR: Negative w-stacking step! Consider increasing the margin!\n");
        exit(1);
    }

}

/**
 * \brief Determine and set the degridding configuration settings.
 * \return True if the operation succeeds in setting the degridding configuration settings, false otherwise.
 */
bool config_set_degrid(struct work_config *cfg,
                       const char *gridder_path, double gridder_x0, int downsample,
                       const char *w_gridder_path, double w_gridder_x0)
{
    if (gridder_path) {

        // Clear existing data, if any
        free(cfg->gridder.data); free(cfg->gridder.corr);
        memset(&cfg->gridder, 0, sizeof(struct sep_kernel_data));

        // Load gridder(s)
        if (load_sep_kern(gridder_path, &cfg->gridder, true)) {
            return false;
        }
        config_log("u/v kernel: support %d (x%d oversampled), %d correction resolution, x0=%g\n",
                   cfg->gridder.size, cfg->gridder.oversampling, cfg->gridder.corr_size, cfg->gridder.x0);
        if (w_gridder_path && w_gridder_path[0]) {
            if (load_sep_kern(w_gridder_path, &cfg->w_gridder, true)) {
                return false;
            }
            config_log("w kernel: support %d (x%d oversampled), %d correction resolution, x0=%g\n",
                   cfg->w_gridder.size, cfg->w_gridder.oversampling,
                   cfg->w_gridder.corr_size, cfg->w_gridder.x0);
        } else {
            cfg->w_gridder.size = 1;
            cfg->w_gridder.oversampling = 1;
        }

        // Override x0
        if (gridder_x0 != 0) {
            cfg->gridder.x0 = gridder_x0;
        }
        if (w_gridder_x0 != 0) {
            cfg->w_gridder.x0 = w_gridder_x0;
        }

        // Reduce oversampling if requested
        if (downsample) {
            cfg->gridder.oversampling /= downsample;
            config_log("Downsampling gridder to %d\n", cfg->gridder.oversampling);
            int i;
            for (i = 1; i < cfg->gridder.oversampling; i++) {
                memcpy(cfg->gridder.data + i * cfg->gridder.stride,
                       cfg->gridder.data + i * downsample * cfg->gridder.stride,
                       sizeof(double) * cfg->gridder.size);
            }
        }

        // Need to rescale grid correction? This is linear, therefore worth a
        // warning. Might want to do a "sinc" interpolation instead at
        // some point? Could be more appropriate.
        const int ncorr = cfg->gridder.corr_size, image_size = cfg->recombine.image_size;
        if (ncorr != image_size) {
            if (ncorr % image_size != 0) {
                config_log("WARNING: Rescaling grid correction from %d to %d points!\n",
                           ncorr, image_size);
            }
            double *old_fn = cfg->gridder.corr;
            cfg->gridder.corr = rescale_fn(old_fn, ncorr, image_size, true);
            cfg->gridder.corr_size = image_size;
            free(old_fn);
        }

    }

    return true;
}

bool config_set_statsd(struct work_config *cfg,
                       const char *node, const char *service)
{
    if (cfg->statsd_socket != -1) close(cfg->statsd_socket);
    cfg->statsd_socket = -1;

    // Resolve statsd address
    struct addrinfo hints, *result;
    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_DGRAM;
    int ret = getaddrinfo(node, service, &hints, &result);
    if (ret != 0) {
        fprintf(stderr, "ERROR: Could not resolve statsd address (%s)", gai_strerror(ret));
        return false;
    }

    // Create socket
    struct addrinfo *addr = NULL;
    for (addr = result; addr; addr = addr->ai_next) {
        // Attempt to create socket
        cfg->statsd_socket = socket(addr->ai_family, addr->ai_socktype, addr->ai_protocol);
        if (cfg->statsd_socket == -1)
            continue;

        // And connect
        if (connect(cfg->statsd_socket, addr->ai_addr, addr->ai_addrlen) != -1)
            break;

        close(cfg->statsd_socket); cfg->statsd_socket = -1;
    }
    if (cfg->statsd_socket == -1) {
        fprintf(stderr, "ERROR: Could not create statsd socket (%s)", strerror(errno));
        freeaddrinfo(result);
        return false;
    }

    freeaddrinfo(result);

    // Initialise stats
    config_log("Opened statsd connection to %s:%s\n", node, service);
    return true;
}

void config_send_statsd(struct work_config *cfg, const char *stat)
{
    if (cfg->statsd_socket == -1)
        return;

    if (write(cfg->statsd_socket, stat, strlen(stat)) != strlen(stat)) {
        fprintf(stderr, "ERROR: Failed to send to statsd (%s)\n", strerror(errno));
        close(cfg->statsd_socket);
        cfg->statsd_socket = -1;
    }
}

void config_load_facets(struct work_config *cfg,
                        const char *path_fmt,
                        const char *hdf5)
{

    int i;
    for (i = 0; i < cfg->facet_workers * cfg->facet_max_work; i++) {
        struct facet_work *work = cfg->facet_work + i;
        if (!work->set) continue;
        char path[256];
        snprintf(path, 256, path_fmt, work->im, work->il);
        work->path = strdup(path);
        work->hdf5 = hdf5 ? strdup(hdf5) : NULL;
    }

}

/**
 * \brief Set the subgrid check data parameters for the subgrid work.
 */
void config_check_subgrids(struct work_config *cfg,
                           double threshold, double fct_threshold,
                           double degrid_threshold,
                           const char *check_fmt,
                           const char *check_fct_fmt,
                           const char *check_degrid_fmt,
                           const char *hdf5)
{
    int i;
    for (i = 0; i < cfg->subgrid_work_count; i++) {
        struct subgrid_work *work = cfg->subgrid_work + i;
        if (!work->nbl) continue;
        char path[256];
        if (check_fmt) {
            snprintf(path, 256, check_fmt, work->iv, work->iu);
            work->check_path = strdup(path);
        }
        if (check_fct_fmt) {
            snprintf(path, 256, check_fct_fmt, work->iv, work->iu);
            work->check_fct_path = strdup(path);
        }
        if (check_degrid_fmt) {
            snprintf(path, 256, check_degrid_fmt, work->iv, work->iu);
            work->check_degrid_path = strdup(path);
        }
        work->check_hdf5 = hdf5 ? strdup(hdf5) : NULL;
        work->check_threshold = threshold;
        work->check_fct_threshold = fct_threshold;
        work->check_degrid_threshold = degrid_threshold;
    }
}

/**
 * \brief Generate facet, subgrid, or full work assignments.
 * \return True if the operation succeeds in generating the relevant work assignment, false otherwise.
 */
bool config_assign_work(struct work_config *cfg,
                        int facet_workers, int subgrid_workers)
{
    cfg->facet_workers = facet_workers;
    cfg->subgrid_workers = subgrid_workers;
    if (cfg->facet_workers < 0) {
        config_log("ERROR: Number of facet workers is negative (%d)!\n", cfg->facet_workers);
        exit(1);
    }
    if (cfg->subgrid_workers < 0) {
        config_log("ERROR: Number of subgrid workers is negative (%d)!\n", cfg->subgrid_workers);
        exit(1);
    }

    // Generate work assignments
    if (cfg->spec.time_count) {
        config_log("\nGenerating work assignments...\n");
        if (!generate_facet_work_assignment(cfg))
            return false;
        if (!generate_subgrid_work_assignment(cfg))
            return false;
    } else {
        if (!generate_full_redistribute_assignment(cfg))
            return false;
    }

    if (cfg->write_inputs) {
        // Write source positions
        write_dump(cfg->source_lmn,
                   3 * sizeof(*cfg->source_lmn) * cfg->source_count,
                   "source_lmn.dmp");
        write_dump(cfg->source_xy,
                   2 * sizeof(*cfg->source_xy) * cfg->source_count,
                   "source_xy.dmp");
	// Dump facets
	const size_t yB_size = cfg->recombine.yB_size;
	const size_t F_size = cfg->recombine.F_size;
	double complex *pF = (double complex *)a_alloc(F_size);
	int ifacet;
        for (ifacet = 0; ifacet < cfg->facet_count; ifacet++) {
	    printf("Dumping facet %d (%lu bytes)...\n", ifacet, F_size);
	    memset(pF, 0, F_size);
	    config_fill_facet(cfg, cfg->facet_work + ifacet, pF, 0, yB_size, 0);
	    write_dump(pF, F_size, "facet%d_%d.dmp",
		       cfg->facet_work[ifacet].il, cfg->facet_work[ifacet].im);
        }
    }

    // Warn if we have multiple facets per worker
    if (cfg->facet_max_work > 1) {
        config_log("WARNING: %d facets, but only %d workers. This means that facets\n"
                   "         compete for send slots, which can cause poor performance\n"
                   "         and dead-lock. Consider more ranks and --facet-workers=%d!\n",
                   cfg->facet_count, cfg->facet_workers, cfg->facet_count+1);
    }

    return true;
}


/**
 * \brief Generate random sources at facets borders
 */
void config_set_sources(struct work_config *cfg, int count, unsigned int seed)
{

    // Determine portion of the image that can be assumed to be
    // precise with respect to our gridding function.
    const int image_size = cfg->recombine.image_size;
    const double image_x0 = cfg->spec.fov / cfg->theta / 2;
    const int image_x0_size = (int)floor(2 * image_x0 * image_size);

    // Allocate
    cfg->source_count = count;
    free(cfg->source_xy); free(cfg->source_lmn); free(cfg->source_corr);
    cfg->source_xy = (double *)malloc(sizeof(double) * count * 2);
    cfg->source_lmn = (double *)malloc(sizeof(double) * count * 3);
    cfg->source_corr = (double *)malloc(sizeof(double) * count);
    cfg->source_energy = (double)count / image_size / image_size;

    // Clear w-kernel (debugging)
    int i;
    //for (i = 0; i < cfg->w_gridder.corr_size; i++) {
    //    cfg->w_gridder.corr[i] = 1.0;
    //}
    //for (i = 0; i < cfg->w_gridder.size; i++) {
    //    cfg->w_gridder.data[i] = 0.0;
    //}
    //cfg->w_gridder.data[cfg->w_gridder.size/2] = 1.0;

    // Start making sources using fixed random seed (so all ranks
    // create the same ones)
    for (i = 0; i < count; i++) {
        // Choose on-grid positions
        int il = (int)(rand_r(&seed) % image_x0_size) - image_x0_size / 2;
        int im = (int)(rand_r(&seed) % image_x0_size) - image_x0_size / 2;
        const int yBs = cfg->recombine.yB_size;
        const int fct_offs = (cfg->facet_count % 2 == 0 ? yBs / 2 : 0);
        switch (rand_r(&seed) % 8) {
            // Worst case for gridder (edge of entire FoV)
        case 0: il=-image_x0_size / 2; break;
        case 1: il=image_x0_size / 2 - 1; break;
        case 2: im=-image_x0_size / 2; break;
        case 3: im=image_x0_size / 2 - 1; break;
            // Worst case for faceting (edge of facets)
        case 4: il = yBs * ((il - fct_offs) / yBs) + yBs / (il>0?2:-2) + fct_offs; break;
        case 5: il = yBs * ((il - fct_offs) / yBs) + yBs / (il>0?2:-2) + fct_offs - 1; break;
        case 6: im = yBs * ((im - fct_offs) / yBs) + yBs / (im>0?2:-2) + fct_offs; break;
        case 7: im = yBs * ((im - fct_offs) / yBs) + yBs / (im>0?2:-2) + fct_offs - 1; break;
        }
        if (il < -image_x0_size / 2) il = -image_x0_size / 2;
        if (il >= image_x0_size / 2) il = image_x0_size / 2 - 1;
        if (im < -image_x0_size / 2) im = -image_x0_size / 2;
        if (im >= image_x0_size / 2) im = image_x0_size / 2 - 1;
        cfg->source_xy[2*i+0] = il; cfg->source_xy[2*i+1] = im;
        // Determine coordinates in l/m/n system
        double l, m, n;
        config_pixel_to_lmn(il, im, image_size / cfg->theta, cfg->shear_u, cfg->shear_v,
                            &l, &m, &n);
        cfg->source_lmn[3*i+0] = l;
        cfg->source_lmn[3*i+1] = m;
        cfg->source_lmn[3*i+2] = n;
        // Get grid correction(s) to apply
        double corr = 1;
        if (cfg->w_gridder.corr) {
            // Note that wstep = cfg->w_gridder.x0 / max_n, so n*wstep
            // is between [-cfg->w_gridder.x0, 0]
            assert(n * cfg->wstep <= 0 &&
                   n * cfg->wstep >= -cfg->w_gridder.x0);
            double corr_pos = -n * cfg->wstep * cfg->w_gridder.corr_size;
            // Get integer and fractional position for linear interpolation
            int ipos = (int)floor(corr_pos); double fpos = corr_pos - ipos;
            double corr0 = cfg->w_gridder.corr[ipos];
            double corr1 = cfg->w_gridder.corr[ipos+1];
            // Determine w-correction
            corr *= corr0 * (1-fpos) + corr1 * fpos;
        }
        if (cfg->gridder.corr) {
            corr *=
                cfg->gridder.corr[(il + image_size) % image_size] *
                cfg->gridder.corr[(im + image_size) % image_size];
        }

        // Could cause divisions by zero down the line
        assert(corr != 0);
        cfg->source_corr[i] = corr;
    }
}

bool create_bl_groups(hid_t vis_group, struct work_config *work_cfg, int worker)
{
    struct vis_spec *spec = &work_cfg->spec;
    struct ant_config *cfg = spec->cfg;

    struct bl_data *bl0 = work_cfg->bl_data;
    hsize_t freq_size = bl0->freq_count;
    hid_t freq_dsp = H5Screate_simple(1, &freq_size, NULL);
    hid_t freq_ds = H5Dcreate(vis_group, "frequency", H5T_IEEE_F64LE,
                              freq_dsp, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    if (freq_ds < 0) {
        fprintf(stderr, "failed to create frequency dataset!");
        H5Sclose(freq_dsp);
        return false;
    }
    H5Dwrite(freq_ds, H5T_NATIVE_DOUBLE, freq_dsp, freq_dsp, H5P_DEFAULT, bl0->freq);
    H5Sclose(freq_dsp); H5Dclose(freq_ds);

    hsize_t time_size = bl0->time_count;
    hid_t time_dsp = H5Screate_simple(1, &time_size, NULL);
    hid_t time_ds = H5Dcreate(vis_group, "time", H5T_IEEE_F64LE,
                              time_dsp, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    if (time_ds < 0) {
        fprintf(stderr, "failed to create time dataset!");
        H5Sclose(time_dsp);
        return false;
    }
    H5Dwrite(time_ds, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT, bl0->time);
    H5Sclose(time_dsp); H5Dclose(time_ds);

    hsize_t uvw_dims[3] = { cfg->ant_count, bl0->time_count, 3 };
    hid_t uvw_dsp = H5Screate_simple(3, uvw_dims, NULL);
    hid_t uvw_ds = H5Dcreate(vis_group, "uvw", H5T_IEEE_F64LE,
                             uvw_dsp, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    if (uvw_ds < 0) {
        fprintf(stderr, "failed to create coordinates dataset!");
        H5Sclose(uvw_dsp); H5Dclose(uvw_ds);
        return false;
    }
    H5Dwrite(uvw_ds, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT, bl0->uvw_m_1);

    int a1, a2;
    int ncreated = 0;
    uint64_t nvis = 0;
    double create_start = get_time_ns();
    for (a1 = 0; a1 < cfg->ant_count; a1++) {
        // Progress message
        if (a1 % 32 == 0) { config_log("%d ", a1); fflush(stdout); }

        hid_t a1_g = 0;
        for (a2 = a1+1; a2 < cfg->ant_count; a2++) {

            // Create outer antenna group, if not already done so
            if (!a1_g) {
                char a1name[12];
                sprintf(a1name, "%d", a1);
                a1_g = H5Gcreate(vis_group, a1name, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
                if (a1_g < 0) {
                    fprintf(stderr, "Could not open '%s' antenna group!\n", a1name);
                    return false;
                }
            }

            // Create inner antenna group
            char a2name[12];
            sprintf(a2name, "%d", a2);
            hid_t a2_g = H5Gcreate(a1_g, a2name, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
            if (a2_g < 0) {
                fprintf(stderr, "Could not open '%s' antenna group!\n", a2name);
                H5Gclose(a1_g);
                return false;
            }

            // Write to visibility group
            struct bl_data *bl = work_cfg->bl_data + a1 + cfg->ant_count*a2;
            if (!create_vis_group(a2_g, spec->freq_chunk, spec->time_chunk, bl)) {
                H5Gclose(a2_g); H5Gclose(a1_g);
                return 1;
            }

            // Statistics & cleanups
            ncreated++;
            nvis += bl->time_count * bl->freq_count;
            H5Gclose(a2_g);
        }
        if (a1_g) H5Gclose(a1_g);
    }

    config_log("\ndone in %.2fs, %d groups for up to %"PRIu64" visibilities (~%.3f GB) created\n",
               get_time_ns() -create_start, ncreated, nvis, 16. * nvis / 1000000000);

    return true;
}

/**
 * Simply send the worker number to the work assignment thread to
 * put us into the queue for receiving work.
 */
void config_request_work(struct work_config *work_cfg, int *worker)
{
    MPI_Request request;
    MPI_Isend(worker, 1, MPI_INT, work_cfg->assign_rank, WORK_REQUEST_TAG,
              work_cfg->assign_comm, &request);

}

static void *config_worker_assignment_thread(void *param)
{
    struct work_config *work_cfg = (struct work_config *)param;
    const bool DEBUG_LOG = false;

    // Get number of ranks
    int ranks;
    MPI_Comm_size(MPI_COMM_WORLD, &ranks);

    // Initialise request queue
    const int queue_length = 1024 * 16;
    MPI_Request *requests = (MPI_Request *)alloca(sizeof(MPI_Request) * queue_length);
    int *indices = (int *)alloca(sizeof(int) * queue_length);
    int i;
    for (i = 0; i < queue_length; i++) {
        requests[i] = MPI_REQUEST_NULL;
    }

    // Find a work item that has not yet been assigned
    int iwork;
    int *workers = malloc(sizeof(int) * work_cfg->subgrid_work_count);
    for (iwork = 0; iwork < work_cfg->subgrid_work_count; iwork++) {

        // Already assigned? Skip
        struct subgrid_work *work = work_cfg->subgrid_work + iwork;
        if (work->worker >= 0) continue;


        // Receive a work request
        MPI_Status status;
        double start = get_time_ns();
        if (MPI_Irecv(workers + iwork, 1, MPI_INT, MPI_ANY_SOURCE,
                      WORK_REQUEST_TAG, work_cfg->assign_comm, requests) != MPI_SUCCESS)
            return NULL;
        do {
            int outcount;
            MPI_Waitsome(queue_length, requests, &outcount, indices, MPI_STATUSES_IGNORE);
        } while(requests[0] != MPI_REQUEST_NULL);
        if (DEBUG_LOG)
            printf("Work %d (tag %d, subgrid %d/%d/%d) assigned to %d (wait %g)\n",
                   iwork, work->assign_tag, work->iu, work->iv, work->iw, workers[iwork], get_time_ns() - start);
        start = get_time_ns();

        // Send out assigned worker. One of this messages is likely
        // going to go to this same process, where it will be written
        // to work->worker - so no point doing it here. That's also
        // why we're not using subgrid_work_assigned.
        int slot = 1;
        for (i = 0; i < ranks; i++) {
            for (; slot < queue_length; slot++)
                if (requests[slot] == MPI_REQUEST_NULL)
                    break;
            if (slot >= queue_length) {
                MPI_Waitany(queue_length-1, requests+1, &slot, &status);
                slot++;
            }
            assert(requests[slot] == MPI_REQUEST_NULL);
            if (MPI_Isend(workers + iwork, 1, MPI_INT, i, /*WORK_REQUEST_TAG + 1*/
                           work->assign_tag, work_cfg->assign_comm, requests+slot)
                != MPI_SUCCESS)
                return NULL;
            slot++;
        }
        if (DEBUG_LOG)
            printf("... notifications sent in %gs\n", get_time_ns() - start);
    }

    return NULL;
}

bool config_dynamic_work_assignment(struct work_config *work_cfg, int work_assign_rank)
{
    // Make communicator for work assignment
    MPI_Comm_dup(MPI_COMM_WORLD, &work_cfg->assign_comm);

    // Start all receives, assigning tags
    int i;
    for (i = work_cfg->subgrid_work_assigned; i < work_cfg->subgrid_work_count; i++) {
        struct subgrid_work *work = work_cfg->subgrid_work + i;

        // Worker is set via asynchronous receive from work assigner
        // (see config_worker_assignment_thread above)
        work->assign_tag = WORK_REQUEST_TAG + 1 + i;
        if (MPI_Irecv(&work->worker, 1, MPI_INT, work_assign_rank, work->assign_tag,
                      work_cfg->assign_comm, &work->assign_request) != MPI_SUCCESS)
            return false;

    }

    // Is this the rank where work gets assigned?
    work_cfg->assign_rank = work_assign_rank;
    int my_rank; MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    if (work_assign_rank == my_rank) {

        // Start the thread!
        pthread_create(&work_cfg->work_assign_thread, NULL, config_worker_assignment_thread, work_cfg);
    }

    return true;
}


bool config_fill_facet(struct work_config *wcfg,
		       struct facet_work *work,
		       double complex *F,
		       int x0_start, int x0_end, double w)
{
    const int source_count = wcfg->source_count;
    const double *const source_xy = wcfg->source_xy;
    const double *const source_lmn = wcfg->source_lmn;
    const double *const source_corr = wcfg->source_corr;
    struct recombine2d_config *cfg = &wcfg->recombine;
    int offset = sizeof(double complex) *x0_start * cfg->yB_size;
    int size = sizeof(double complex) *(x0_end - x0_start) * cfg->yB_size;

    if (work->path && !work->hdf5) {

        printf("Reading facet data from %s (%d-%d)...\n", work->path, x0_start, x0_end);

        // Make sure strides are compatible
        assert (cfg->F_stride0 == cfg->yB_size && cfg->F_stride1 == 1);

        // Load data from file
        int fd = open(work->path, O_RDONLY, 0666);
        if (fd > 0) {
            lseek(fd, offset, SEEK_SET);
            if (read(fd, F, size) != size) {
                fprintf(stderr, "failed to read enough data from %s for range %d-%d!\n", work->path, x0_start, x0_end);
                return false;
            }
            close(fd);
        } else {
            fprintf(stderr, "Failed to read facet data!\n");
        }

    } else if (work->path && work->hdf5) {

        printf("Reading facet data from %s:%s (%d-%d)...\n", work->hdf5, work->path, x0_start, x0_end);

        // Make sure strides are as expected, then read
        // TODO: Clearly HDF5 can do partial reads, optimise
        assert (cfg->F_stride0 == cfg->yB_size && cfg->F_stride1 == 1);
        double complex *data;
#pragma omp critical
        data = read_hdf5(cfg->F_size, work->hdf5, work->path);

        // Copy
        memcpy(F, data + offset / sizeof(double complex), size);
        free(data);

    } else if (source_count > 0) {

        // Place sources in gridder's usable region
        int i;
        for (i = 0; i < source_count; i++) {
            int il = source_xy[i*2+0], im = source_xy[i*2+1];

            // Skip sources outside the current facet (region)
            if (il - work->facet_off_l < -cfg->yB_size/2 ||
                il - work->facet_off_l >= cfg->yB_size/2 ||
                im - work->facet_off_m < -cfg->yB_size/2 ||
                im - work->facet_off_m >= cfg->yB_size/2) {

                continue;
            }

            // Calculate facet coordinates, keeping in mind that the
            // centre is at (0/0).
            int x0 = (im - work->facet_off_m + cfg->yB_size) % cfg->yB_size;
            int x1 = (il - work->facet_off_l + cfg->yB_size) % cfg->yB_size;
            if (x0 < x0_start || x0 >= x0_end) {
                continue;
            }

            // Calculate Fresnel pattern for w-stacking
            complex double fresnel = 1;
            if (w != 0) {
                double ph = w * source_lmn[i*3+2];
                fresnel = cos(2*M_PI*ph) + 1.j * sin(2*M_PI*ph);
            }

            // Add source, with gridding correction applied
            F[(x0-x0_start)*cfg->F_stride0 + x1*cfg->F_stride1]
                += fresnel / source_corr[i];
        }

    } else {

        // Fill facet with deterministic pseudo-random numbers
        int x0, x1;
        for (x0 = x0_start; x0 < x0_end; x0++) {
            unsigned int seed = x0;
            for (x1 = 0; x1 < cfg->yB_size; x1++) {
                F[(x0-x0_start)*cfg->F_stride0+x1*cfg->F_stride1] = (double)rand_r(&seed) / RAND_MAX;
            }
        }

    }
    return true;
}
