
#define _ISOC11_SOURCE
#define _DEFAULT_SOURCE

#include "recombine.h"
#include "grid.h"

#include <stdlib.h>
#include <assert.h>
#include <math.h>
#include <time.h>
#include <omp.h>
#include <sys/stat.h>
#ifdef __SSE4_1__
#include <immintrin.h>
#endif

// Fortran routines (see specfun.f)
extern void segv_(int*,int*,double*,int*,double*,double*);
extern void aswfa_(int*,int*,double*,double*,int*,double*,double*,double*);

/**
 * \brief Generate prolate spheroidal wave function (PSWF) at facet resolution
 */
double *generate_pswf(int yN_size, int m, double c)
{

    // Calculate Characteristic values of spheroidal wave functions
    int n = m;
    int kd = 1; // prolate
    double cv, eg[2];
    segv_(&m,&n,&c,&kd,&cv,eg);

    // Now allocate + loop through values to generate
    double *pswf = a_alloc(sizeof(double) * yN_size);

    // Get value at 0 - we normalise this to 1
    double x = 0;
    double s1f_norm, s1d_norm;
    aswfa_(&m,&n,&c,&x,&kd,&cv,&s1f_norm,&s1d_norm);
    pswf[0] = s1f_norm;
    pswf[yN_size / 2] = 0.0;

    // Get remaining values
    for(int i = 1; i < yN_size / 2; i++) {
        double x = 2 * ((double)i) / yN_size;

        // Get value (plus derivative)
        double s1f, s1d;
        aswfa_(&m,&n,&c,&x,&kd,&cv,&s1f,&s1d);
        pswf[i] = s1f;
        pswf[yN_size - i] = s1f;

    }

    return pswf;
}

/**
 * \brief Generate facet mask (at facet resolution), multiplied by inverse of PSWF.
 */
double *generate_Fb(int yN_size, int yB_size, int yP_size, double *pswf) {
    double *Fb = (double *)a_alloc(sizeof(double) * yB_size);
    int i;
    for (i = 0; i <= yB_size/2; i++) {
        Fb[i] = 1 / pswf[i] / yP_size;
    }
    for (i = yB_size/2+1; i < yB_size; i++) {
        Fb[i] = Fb[yB_size-i];
    }
    return Fb;
}

/**
 * \brief Generate PSWF convolution (in image space), to be applied to contribution terms.
 */
double *generate_Fn(int yN_size, int xM_yN_size, double *pswf)
{
    double *Fn = (double *)a_alloc(sizeof(double) * xM_yN_size);
    int i;
    assert(yN_size % xM_yN_size == 0);
    int xM_step = yN_size / xM_yN_size;
    for (i = 0; i < xM_yN_size; i++) {
        Fn[i] = pswf[i * xM_step];
    }
    return Fn;
}

/**
 * \brief Generate subgrid mask.
 */
double *generate_m(int image_size, int yP_size, int yN_size, int xM_size, int xMxN_yP_size,
                   double *pswf)
{
    double *m = (double *)a_alloc(sizeof(double) * (yP_size/2+1));

    // Fourier transform of rectangular function with size xM, truncated by PSWF
    m[0] = pswf[0] * xM_size / image_size;
    int i;
    for (i = 1; i < yN_size/2; i++) {
        double x = (double)(i) * xM_size / image_size;
        m[i] = pswf[i] * (sin(x * M_PI) / x / M_PI) * xM_size / image_size;
    }
    for (; i <= yP_size / 2; i++) {
        m[i] = 0;
    }

    // Back to grid space
    fftw_plan plan = fftw_plan_r2r_1d(yP_size/2+1, m, m, FFTW_REDFT00, FFTW_ESTIMATE);
    fftw_execute(plan);
    fftw_destroy_plan(plan);

    // Copy and mirror
    double *m_r = (double *)a_alloc(sizeof(double) * xMxN_yP_size);
    for (i = 0; i <= xMxN_yP_size/2; i++) {
        m_r[i] = m[i];
    }
    for (; i < xMxN_yP_size; i++) {
        m_r[i] = m[xMxN_yP_size-i];
    }
    free(m);

    return m_r;
}

/**
 * \brief Pad / oversample facet and multiply by b
 */
void prepare_facet(int yB_size, int yP_size,
                   double *Fb,
                   double complex *facet, int facet_stride,
                   double complex *BF, int BF_stride)
{
#ifdef __AVX2__
    if (BF_stride == 1 && facet_stride == 1) {
       assert((((intptr_t)facet) % 32) == 0);
       assert((((intptr_t)BF) % 32) == 0);
       assert((((intptr_t)Fb) % 32) == 0);
       assert(yP_size % 4 == 0);
       assert(yB_size % 8 == 0);
       // Optimised version. Doesn't buy more than a couple percent.
       int i = 0;
       for (; i < yB_size/2; i+=4) {
           __m256d f = _mm256_load_pd((double *)(facet + i));
           __m256d b = _mm256_load_pd((double *)(Fb + i));
           _mm256_store_pd((double *)(BF + i), f * _mm256_permute4x64_pd(b,80));
           f = _mm256_load_pd((double *)(facet + i + 2));
           _mm256_store_pd((double *)(BF + i + 2), f * _mm256_permute4x64_pd(b,250));
       }
       for (; i < yP_size-yB_size/2; i++) {
           BF[i] = 0;
       }
       for (; i < yP_size; i+=4) {
           __m256d f = _mm256_load_pd((double *)(facet + yB_size-yP_size+i));
           __m256d b = _mm256_load_pd((double *)(Fb + yB_size-yP_size+i));
           _mm256_store_pd((double *)(BF + i), f * _mm256_permute4x64_pd(b,80));
           f = _mm256_load_pd((double *)(facet + + yB_size-yP_size+i + 2));
           _mm256_store_pd((double *)(BF + i + 2), f * _mm256_permute4x64_pd(b,250));
       }
       return;
    }
#endif
    // Multiply by Fb, pad up to yP
    int i;
    for (i = 0; i < yB_size/2; i++) {
        BF[BF_stride*i] = Fb[i] * facet[facet_stride*i];
    }
    for (; i < yP_size-yB_size/2; i++) {
        BF[BF_stride*i] = 0;
    }
    for (; i < yP_size; i++) {
        BF[BF_stride*i] = Fb[yB_size-yP_size+i] * facet[facet_stride*(yB_size-yP_size+i)];
    }
}

/**
 * \brief Extract contribution of a (prepared+FFT'd) facet to a subgrid at a given offset
 */
void extract_subgrid(int yP_size, int xM_yP_size, int xMxN_yP_size, int xM_yN_size, int subgrid_offset,
                     double *m_trunc, double *Fn,
                     complex double *BF, int BF_stride,
                     complex double *MBF, fftw_plan MBF_plan,
                     complex double *NMBF, int NMBF_stride)
{
    int i;
    int xN_yP_size = xMxN_yP_size - xM_yP_size;
    assert(xN_yP_size % 2 == 0); // re-check loop borders...
    subgrid_offset += 2 * yP_size; assert(subgrid_offset >= xM_yP_size);
    if (xM_yP_size != xM_yN_size) {
        // m * b, with xN_yP_size worth of margin looping around the sides
        for (i = 0; i < xM_yP_size - xMxN_yP_size / 2; i++) {
            MBF[i] = m_trunc[i] * BF[BF_stride * ((i + subgrid_offset) % yP_size)];
        }
        for (; i < (xMxN_yP_size + 1) / 2; i++) {
            MBF[i] = m_trunc[i] * BF[BF_stride * ((i + subgrid_offset) % yP_size)];
            int bf_ix = i + subgrid_offset - xM_yP_size;
            MBF[i] += m_trunc[xN_yP_size+i] * BF[BF_stride * (bf_ix % yP_size)];
        }
        for (; i < xM_yP_size; i++) {
            int bf_ix = i + subgrid_offset - xM_yP_size;
            MBF[i] = m_trunc[xN_yP_size+i] * BF[BF_stride * (bf_ix % yP_size)];
        }
    } else {
        // If yP = yN we can simply copy here - note that we
        // especially don't actually need the "m" mask.
        for (i = 0; i < xM_yP_size / 2; i++) {
            MBF[i] = BF[BF_stride * ((i + subgrid_offset) % yP_size)];
        }
        for (; i < xM_yP_size; i++) {
            MBF[i] = BF[BF_stride * ((i + subgrid_offset - xM_yP_size) % yP_size)];
        }
    }
    fftw_execute(MBF_plan);
    for (i = 0; i < (xM_yN_size + 1) / 2; i++) {
        NMBF[i * NMBF_stride] = MBF[i] * Fn[i];
    }
    for (; i < xM_yN_size; i++) {
        NMBF[i * NMBF_stride] = MBF[xM_yP_size-xM_yN_size+i] * Fn[i];
    }
}

/**
 * \brief Generic kernel.
 */
void add_facet(int xM_size, int xM_yN_size, int facet_offset,
               complex double *NMBF, int NMBF_stride,
               complex double *out, int out_stride)
{
    int i;
    facet_offset += 2 * xM_size; assert(facet_offset >= xM_yN_size/2);
    for (i = 0; i < xM_yN_size; i++) {
        out[out_stride * ((i - xM_yN_size/2 + facet_offset) % xM_size)] +=
            NMBF[NMBF_stride * ((i - xM_yN_size/2 + xM_yN_size) % xM_yN_size)] / xM_size;
    }

}

double calc_error(int N, int yN_size, int xA_size, int xM_size,
                  double *pswf, double complex *gw, int y_source, bool double_n)
{
    // Plan here is to calculate the grid value at point -xM of A*n*n,
    // i.e. the convolution (image space multiplication) of the facet
    // mask with (twice) the windowing function. This essentially
    // gives us an idea how quickly the PSWF suppresses the signal
    // towards the subgrid edges.

    // If gw is given, it is multiplied in additionally
    // (non-coplanarity factor).

    double complex sum = 0;
    //printf("N=%d yN_size=%d xA_size=%d xM_size=%d\n", N, yN_size, xA_size, xM_size);
    int i;
    for (i = 0; i < yN_size; i++) {
        int y = i - yN_size/2;
        // "A" in image space is a sinc. We can change the position of
        // the "test" source.
        double Ay = (y - y_source) * (M_PI * xA_size / N);
        double FA = Ay ? sin(Ay) / Ay : 1;
        // Windowing function goes in squared normally (to include
        // effect of masking with the "smoothed" m of above), but
        // skipping it is useful for estimating the base error level
        // (see below).
        int pi = (i + yN_size/2) % yN_size;
        double p = pswf[pi]; if (double_n) p *= p;
        // The -xM offset is a complex exponential
        double offs_ph = -2 * y * (M_PI * (xM_size * yN_size / N / 2) / yN_size);
        double offs_r = cos(offs_ph) / yN_size, offs_i = sin(offs_ph) / yN_size;
        //printf("%d\t%g\t%g\t%g\t%g%+gj\n", y, pswf[pi] * pswf[pi], FA, offs_ph, offs_r, offs_i);
        double complex g = gw ? gw[pi] : 1;
        sum += p * FA * (offs_r + I * offs_i) * g;
    }

    // We want the absolute
    return sqrt(creal(sum)*creal(sum)+cimag(sum)*cimag(sum));
}

double estimate_error(int N, int yN_size, int yB_size, int xA_size, int xM_size, double *pswf)
{
    // Determine error without the PSWF squared for a source at
    // yN_size / 2 - this allows us to measure the base error level.
    double err = calc_error(N, yN_size, xA_size, xM_size, pswf, NULL, yN_size/2, false);

    // Multiply by 1/pswf at yB_size/2 to get the error estimation
    return err / pswf[yB_size / 2];
}

int estimate_max_w_planes(int N, int yN_size, int yB_size, int xA_size, int xM_size,
                          double *pswf, double w_step, double theta, double fov, double target_err,
                          bool double_n)
{
    // If w != 0, we also add a non-coplanarity factor. The idea is
    // that lG represents the size of the image and fov the true size
    // of the field of view (both direction cosines, so 2 maximum).

    // Make base gw step pattern
    double complex *gw = (double complex *)a_alloc(sizeof(double complex) * yN_size * 2);
    double complex *gw_step = gw + yN_size;
    int i;
    for (i = 0; i < yN_size; i++) {
        // l coordinates should assume a facet at the very border of
        // the field of view for maximally bad w-pattern. This means
        // starting at -fov/2, then step at pixel resolution (theta/N)
        int il = (i + yN_size / 2) % yN_size;
        double l = -fov/2 + il * theta / N;
        // Calculate g_wstep = e^{2\pi i w_step sqrt(1-l^2)}
        double gw_ph = 2 * M_PI * w_step * sqrt(1 - l * l);
        double gw_r = cos(gw_ph), gw_i = sin(gw_ph);
        gw_step[i] = gw_r + I * gw_i;
        //printf("%g%+gj\n", gw_r, gw_i);
        gw[i] = 1.0;
    }

    // Determine base error to shoot for
    double base_err = target_err * pswf[yB_size / 2];

    // Walk through possible w-values (simple binary search)
    int iw = 0; int idw = 8;
    while (true) {

        // Determine error with current w-pattern and a source in the
        // middle (yN_size/2 source position is only worst case at
        // w=0!)
        double err = calc_error(N, yN_size, xA_size, xM_size, pswf, gw, 0, double_n);
        //printf("w=%g err=%g\n", iw*w_step, err);

        // Arrived at minimum step length? Finished
        if (idw == 1) {
            return (err > base_err ? iw - 1 : iw);
        }

        // Error not acceptable? Reduce step length
        if (err > base_err) idw /= 2;

        // Error acceptable? Make a step of the currently configured length
        int i;
        for (i = 0; i < yN_size; i++) {
            double complex g = gw_step[i];
            int j;
            for (j = 1; j < idw; j *= 2) g = g * g;
            gw[i] *= (err > base_err ? conj(g) : g);
        }
        iw += (err > base_err ? -idw : idw);

        // If this trips, the original subgrid size xA_size was
        // estimated to not be enough for the target error, and we
        // were about to icnrease it past the given maximum.
        assert (iw > 0);
    }
}

void *rescale_fn(double *fn, int size, int new_size, bool as_log)
{
    double *new_fn = (double *)a_alloc(sizeof(double) * new_size);
    int i;
    for (i = 0; i < new_size; i++) {
        double j = (double)i * size / new_size;
        int j0 = (int)floor(j),  j1 = (j0 + 1) % size;
        double w = j - j0;
        if (w < 1e-15)
            new_fn[i] = fn[j0];
        // linear inerpolation
        else if (!as_log)
            new_fn[i] = (1 - w) * fn[j0] + w * fn[j1];
        // log-linear interpolation
        else
            new_fn[i] = exp((1 - w) * log(fn[j0]) + w * log(fn[j1]));
    }
    return new_fn;
}


bool recombine2d_set_config(struct recombine2d_config *cfg,
                            int image_size, int subgrid_spacing,
                            char *pswf_file, double W,
                            int yB_size, int yN_size, int yP_size,
                            int xA_size, int xM_size, int xMxN_yP_size)
{

    cfg->stream_check = NULL;
    cfg->stream_check_threshold = 0;
    cfg->stream_dump = NULL;

    cfg->image_size = image_size;
    cfg->subgrid_spacing = subgrid_spacing;
    assert(image_size % subgrid_spacing == 0);
    cfg->facet_spacing = image_size / subgrid_spacing;

    cfg->yB_size    = yB_size;
    cfg->yN_size    = yN_size;
    cfg->yP_size    = yP_size;
    cfg->xA_size    = xA_size;
    cfg->xM_size    = xM_size;
    cfg->xMxN_yP_size = xMxN_yP_size;

    // Check some side conditions...
    assert(image_size % xM_size == 0);
    int xM_step = image_size / xM_size;
    assert(cfg->facet_spacing % xM_step == 0);
    assert((cfg->subgrid_spacing * cfg->yP_size) % image_size == 0);

    // Only needed if we want to tile facets+subgrids without overlaps.
    // Could be generalised once we get smarter about this.
    assert(cfg->yB_size % cfg->facet_spacing == 0);
    assert(cfg->xA_size % cfg->subgrid_spacing == 0);

    cfg->yP_spacing = cfg->subgrid_spacing * cfg->yP_size / image_size;
    cfg->xM_spacing = cfg->facet_spacing * cfg->xM_size / image_size;
    assert((cfg->xM_size * cfg->yP_size) % cfg->image_size == 0);
    cfg->xM_yP_size = cfg->xM_size * cfg->yP_size / cfg->image_size;
    assert((cfg->xM_size * cfg->yN_size) % cfg->image_size == 0);
    cfg->xM_yN_size = cfg->xM_size * cfg->yN_size / cfg->image_size;

    cfg->F_size = sizeof(double complex) * cfg->yB_size * cfg->yB_size;
    cfg->BF_size = sizeof(double complex) * cfg->yP_size * cfg->yB_size;
    cfg->MBF_size = sizeof(double complex) * cfg->xM_yP_size;
    cfg->NMBF_size = sizeof(double complex) * cfg->xM_yN_size * cfg->yB_size;
    cfg->NMBF_BF_size = sizeof(double complex) * cfg->xM_yN_size * cfg->yP_size;
    cfg->NMBF_NMBF_size = sizeof(double complex) * cfg->xM_yN_size * cfg->xM_yN_size;

    cfg->SG_size = sizeof(double complex) * cfg->xM_size * cfg->xM_size;

    cfg->F_stride0 = cfg->yB_size; cfg->F_stride1 = 1;
    cfg->BF_stride0 = cfg->yP_size; cfg->BF_stride1 = 1;
    cfg->NMBF_stride0 = cfg->xM_yN_size; cfg->NMBF_stride1 = 1;
    cfg->NMBF_BF_stride0 = 1; cfg->NMBF_BF_stride1 = cfg->yP_size;
    cfg->NMBF_NMBF_stride0 = cfg->xM_yN_size; cfg->NMBF_NMBF_stride1 = 1;

    // If W is given: Generate PSWF
    if (W != 0) {
        cfg->pswf = generate_pswf(cfg->yN_size, 0, M_PI * W / 2);

    } else {

        // Read PSWF
        struct stat st;
        if (stat(pswf_file, &st)) { printf("Could not access PSWF file %s!\n", pswf_file); return false; }
        cfg->pswf = read_dump(st.st_size, pswf_file);
        if (!cfg->pswf) return false;

        // PSWF rescaling needed?
        if (st.st_size != sizeof(double) * cfg->yN_size) {
            // This can lose us some actual precision unless the sampling
            // ratio is a multiple (which we normally should have made
            // sure of - not that it matters *that* much).
            if (st.st_size % (sizeof(double) * cfg->yN_size) != 0) {
                printf("WARNING: Rescaling pswf from %lu to %d points!\n",
                       st.st_size / sizeof(double), cfg->yN_size);
            }
            // Perform the rescaling
            double *pswf_new = rescale_fn(cfg->pswf, st.st_size / sizeof(double), cfg->yN_size, true);
            free(cfg->pswf);
            cfg->pswf = pswf_new;
        }
    }

    // Generate Fn, Fb and m
    cfg->Fb = generate_Fb(cfg->yN_size, cfg->yB_size, cfg->yP_size, cfg->pswf);
    cfg->Fn = generate_Fn(cfg->yN_size, cfg->xM_yN_size, cfg->pswf);
    cfg->m = generate_m(image_size, cfg->yP_size, cfg->yN_size, cfg->xM_size, cfg->xMxN_yP_size, cfg->pswf);

    return true;
}

void recombine2d_free(struct recombine2d_config *cfg)
{
    free(cfg->pswf); free(cfg->Fb); free(cfg->Fn); free(cfg->m);
}

// Global/per-thread memory required to run producer side
uint64_t recombine2d_global_memory(struct recombine2d_config *cfg)
{
    return cfg->F_size + cfg->BF_size;
}
uint64_t recombine2d_worker_memory(struct recombine2d_config *cfg)
{
    return cfg->MBF_size + cfg->NMBF_size + cfg->NMBF_BF_size + cfg->NMBF_NMBF_size;
}

/**
 * \return Get the time in nanoseconds.
 */
static double get_time_ns()
{
    struct timespec ts;
    clock_gettime(CLOCK_REALTIME, &ts);
    return ts.tv_sec + (double)ts.tv_nsec / 1000000000;
}

fftw_plan recombine2d_bf_plan(struct recombine2d_config *cfg, int BF_batch,
                              double complex *BF, unsigned planner_flags)
{
    return fftw_plan_many_dft(1, &cfg->yP_size, BF_batch,
                              BF, 0, cfg->BF_stride1, cfg->BF_stride0,
                              BF, 0, cfg->BF_stride1, cfg->BF_stride0,
                              FFTW_BACKWARD, planner_flags);
}

void recombine2d_init_worker(struct recombine2d_worker *worker, struct recombine2d_config *cfg,
                             int BF_batch, fftw_plan BF_plan, unsigned planner_flags)
{
    // Set configuration
    worker->cfg = cfg;

    // Create buffers
    worker->MBF = (double complex *)a_alloc(cfg->MBF_size);
    worker->NMBF = (double complex *)a_alloc(cfg->NMBF_size);
    worker->NMBF_BF = (double complex *)a_alloc(cfg->NMBF_BF_size);

    // Plan Fourier Transforms
    worker->BF_batch = BF_batch; worker->BF_plan = BF_plan;
    worker->MBF_plan = fftw_plan_dft_1d(cfg->xM_yP_size, worker->MBF, worker->MBF,
                                        FFTW_FORWARD, planner_flags);
    worker->NMBF_BF_plan = fftw_plan_many_dft(1, &cfg->yP_size, cfg->xM_yN_size,
                                            worker->NMBF_BF, 0, cfg->NMBF_BF_stride0, cfg->NMBF_BF_stride1,
                                            worker->NMBF_BF, 0, cfg->NMBF_BF_stride0, cfg->NMBF_BF_stride1,
                                            FFTW_BACKWARD, planner_flags);

    // Get entertaining FLOP numbers
    double fft_add, fft_mul, fft_fma;
    fftw_flops(worker->BF_plan, &fft_add, &fft_mul, &fft_fma);
    worker->BF_plan_flops = fft_add + fft_mul + 2 * fft_fma;
    fftw_flops(worker->MBF_plan, &fft_add, &fft_mul, &fft_fma);
    worker->MBF_plan_flops = fft_add + fft_mul + 2 * fft_fma;
    fftw_flops(worker->NMBF_BF_plan, &fft_add, &fft_mul, &fft_fma);
    worker->NMBF_BF_plan_flops = fft_add + fft_mul + 2 * fft_fma;

    // Facet preparation floating point operations: 2 per facet size (real-complex multiplication)
    worker->pf_flops = cfg->yB_size * 2;

    // Subgrid extraction floating point operations:
    // * 2 flops per xMxN_yP_size (complex-complex addition)
    // * MBF FFT
    // * 2 flops per xM_yN_size (real-complex multiplication)
    worker->es_flops = 2 * cfg->xMxN_yP_size +  worker->MBF_plan_flops + 2 * cfg->xM_yN_size;

    // Initialise statistics
    worker->pf1_time = worker->es1_time = worker->ft1_time =
        worker->pf2_time = worker->es2_time = worker->ft2_time = 0;
    worker->pf1_flops = worker->es1_flops = worker->ft1_flops =
        worker->pf2_flops = worker->es2_flops = worker->ft2_flops = 0;

}

void recombine2d_free_worker(struct recombine2d_worker *worker)
{
    // (BF_plan is assumed to be shared)
    fftw_destroy_plan(worker->MBF_plan);
    fftw_destroy_plan(worker->NMBF_BF_plan);

    free(worker->MBF);
    free(worker->NMBF);
    free(worker->NMBF_BF);
}

void recombine2d_pf1_ft1_omp(struct recombine2d_worker *worker,
                             complex double *F,
                             complex double *BF)
{
    struct recombine2d_config *cfg = worker->cfg;
    int y;
#pragma omp for schedule(dynamic)
    for (y = 0; y < cfg->yB_size; y+=worker->BF_batch) {

        // Facet preparation along first axis
        double start = get_time_ns();
        int y2;
        for (y2 = y; y2 < y+worker->BF_batch && y2 < cfg->yB_size; y2++) {
            prepare_facet(cfg->yB_size, cfg->yP_size, cfg->Fb,
                          F+y2*cfg->F_stride0, cfg->F_stride1,
                          BF+y2*cfg->BF_stride0, cfg->BF_stride1);
        }
        worker->pf1_time += get_time_ns() - start;
        worker->pf1_flops += (y2 - y) * worker->pf_flops;

        // Fourier transform along first axis
        start = get_time_ns();
        fftw_execute_dft(worker->BF_plan, BF+y*cfg->BF_stride0, BF+y*cfg->BF_stride0);
        worker->ft1_time += get_time_ns() - start;
        worker->ft1_flops += worker->BF_plan_flops;
    }

}

void recombine2d_pf1_ft1_es1_omp(struct recombine2d_worker *worker,
                                 int subgrid_off1,
                                 complex double *F,
                                 complex double *NMBF)
{
    struct recombine2d_config *cfg = worker->cfg;
    int y;

    int BF_chunk_size = sizeof(double complex) * cfg->yP_size * worker->BF_batch;
    double complex *BF_chunk = a_alloc(BF_chunk_size);
    assert(cfg->BF_stride1 == 1);
    assert(cfg->NMBF_BF_stride0 == 1);
    assert(cfg->BF_stride0 == cfg->NMBF_BF_stride1);
    assert(subgrid_off1 % cfg->subgrid_spacing == 0);

#pragma omp for schedule(dynamic)
    for (y = 0; y < cfg->yB_size; y+=worker->BF_batch) {

        // Facet preparation along first axis
        double start = get_time_ns();
        int y2;
        for (y2 = y; y2 < y+worker->BF_batch && y2 < cfg->yB_size; y2++) {
            prepare_facet(cfg->yB_size, cfg->yP_size, cfg->Fb,
                          F+y2*cfg->F_stride0, cfg->F_stride1,
                          BF_chunk+(y2-y)*cfg->BF_stride0, cfg->BF_stride1);
        }
        worker->pf1_time += get_time_ns() - start;
        worker->pf1_flops += (y2 - y) * worker->pf_flops;

        // Fourier transform along first axis
        start = get_time_ns();
        fftw_execute_dft(worker->BF_plan, BF_chunk, BF_chunk);
        worker->ft1_time += get_time_ns() - start;
        worker->ft1_flops += worker->BF_plan_flops;

        // Extract subgrids along first axis
        assert(subgrid_off1 % cfg->subgrid_spacing == 0);
        int subgrid_offset = subgrid_off1 / cfg->subgrid_spacing * cfg->yP_spacing;
        start = get_time_ns();
        for (y2 = y; y2 < y+worker->BF_batch && y2 < cfg->yB_size; y2++) {
            extract_subgrid(cfg->yP_size, cfg->xM_yP_size, cfg->xMxN_yP_size, cfg->xM_yN_size,
                            subgrid_offset, cfg->m, cfg->Fn,
                            BF_chunk+(y2-y)*cfg->BF_stride0, cfg->BF_stride1,
                            worker->MBF, worker->MBF_plan,
                            NMBF+y2*cfg->NMBF_stride0, cfg->NMBF_stride1);
        }
        worker->es1_time += get_time_ns() - start;
        worker->es1_flops += (y2 - y) * worker->es_flops;
    }

    free(BF_chunk);
}

void recombine2d_es1_pf0_ft0(struct recombine2d_worker *worker,
                             int subgrid_off1, complex double *BF, double complex *NMBF_BF)
{
    struct recombine2d_config *cfg = worker->cfg;
    int x,y;

    // Extract subgrids along first axis
    assert(subgrid_off1 % cfg->subgrid_spacing == 0);
    int subgrid_offset = subgrid_off1 / cfg->subgrid_spacing * cfg->yP_spacing;
    double start = get_time_ns();
    for (x = 0; x < cfg->yB_size; x++) {
        extract_subgrid(cfg->yP_size, cfg->xM_yP_size, cfg->xMxN_yP_size, cfg->xM_yN_size,
                        subgrid_offset, cfg->m, cfg->Fn,
                        BF+x*cfg->BF_stride0, cfg->BF_stride1,
                        worker->MBF, worker->MBF_plan,
                        worker->NMBF+x*cfg->NMBF_stride0, cfg->NMBF_stride1);
    }
    worker->es1_time += get_time_ns() - start;
    worker->es1_flops += x * worker->es_flops;

    // Facet preparation along second axis
    start = get_time_ns();
    for (y = 0; y < cfg->xM_yN_size; y++) {
        prepare_facet(cfg->yB_size, cfg->yP_size, cfg->Fb,
                      worker->NMBF+y*cfg->NMBF_stride1, cfg->NMBF_stride0,
                      NMBF_BF+y*cfg->NMBF_BF_stride1, cfg->NMBF_BF_stride0);
    }
    worker->pf2_time += get_time_ns() - start;
    worker->pf2_flops += cfg->xM_yN_size * worker->pf_flops;

    // Fourier transform along second axis
    start = get_time_ns();
    if (NMBF_BF == worker->NMBF_BF)
        fftw_execute(worker->NMBF_BF_plan);
    else
        fftw_execute_dft(worker->NMBF_BF_plan, NMBF_BF, NMBF_BF);
    worker->ft2_time += get_time_ns() - start;
    worker->ft2_flops += worker->NMBF_BF_plan_flops;

}

void recombine2d_es1_omp(struct recombine2d_worker *worker,
                         int subgrid_off1,
                         complex double *BF,
                         double complex *NMBF)
{
    struct recombine2d_config *cfg = worker->cfg;
    int x;

    // Extract subgrids along first axis
    assert(subgrid_off1 % cfg->subgrid_spacing == 0);
    int subgrid_offset = subgrid_off1 / cfg->subgrid_spacing * cfg->yP_spacing;;
    double start = get_time_ns();
#pragma omp for schedule(dynamic, worker->BF_batch)
    for (x = 0; x < cfg->yB_size; x++) {
        extract_subgrid(cfg->yP_size, cfg->xM_yP_size, cfg->xMxN_yP_size, cfg->xM_yN_size,
                        subgrid_offset, cfg->m, cfg->Fn,
                        BF+x*cfg->BF_stride0, cfg->BF_stride1,
                        worker->MBF, worker->MBF_plan,
                        NMBF+x*cfg->NMBF_stride0, cfg->NMBF_stride1);
        worker->es1_flops += worker->es_flops;
    }
    worker->es1_time += get_time_ns() - start;

}

void recombine2d_pf0_ft0_omp(struct recombine2d_worker *worker,
                             double complex *NMBF,
                             double complex *NMBF_BF)
{
    struct recombine2d_config *cfg = worker->cfg;

    assert(cfg->BF_stride1 == 1);
    assert(cfg->NMBF_BF_stride0 == 1);
    assert(cfg->BF_stride0 == cfg->NMBF_BF_stride1);

    int y;
#pragma omp for schedule(dynamic)
    for (y = 0; y < cfg->xM_yN_size; y+=worker->BF_batch) {

        // Facet preparation along second axis
        double start = get_time_ns();
        int y2;
        for (y2 = y; y2 < y+worker->BF_batch && y2 < cfg->xM_yN_size; y2++) {
            prepare_facet(cfg->yB_size, cfg->yP_size, cfg->Fb,
                          NMBF+y2*cfg->NMBF_stride1, cfg->NMBF_stride0,
                          NMBF_BF+y2*cfg->NMBF_BF_stride1, cfg->NMBF_BF_stride0);
        }
        worker->pf2_time += get_time_ns() - start;
        worker->pf2_flops += (y2 - y) * worker->pf_flops;

        // Fourier transform along second axis.

        // Note 1: We are re-using the BF FFTW plan, which happens to
        // work because we switched strides (see assertions at start
        // of routine).

        // Note 2: We do not want to assume that xM_yN_size gets
        // evenly divided by BF_batch, the quick hack here is to just
        // make an on-the-fly plan for the last bit
        start = get_time_ns();
        fftw_plan plan = worker->BF_plan;
        uint64_t ft_flops = worker->BF_plan_flops;
        if (y+worker->BF_batch >= cfg->xM_yN_size) {
            plan = recombine2d_bf_plan(worker->cfg, cfg->xM_yN_size - y,
                                       NMBF_BF+y*cfg->NMBF_BF_stride1,
                                       FFTW_ESTIMATE);
            double fft_add, fft_mul, fft_fma;
            fftw_flops(plan, &fft_add, &fft_mul, &fft_fma);
            ft_flops = fft_add + fft_mul + 2 * fft_fma;
        }
        fftw_execute_dft(plan,
                         NMBF_BF+y*cfg->NMBF_BF_stride1,
                         NMBF_BF+y*cfg->NMBF_BF_stride1);
        if (plan != worker->BF_plan)
            fftw_destroy_plan(plan);
        worker->ft2_time += get_time_ns() - start;
        worker->ft2_flops += ft_flops;

    }
}

void recombine2d_es0(struct recombine2d_worker *worker,
                     int subgrid_off0, int subgrid_off1,
                     double complex *NMBF_BF,
                     double complex *NMBF_NMBF)
{
    struct recombine2d_config *cfg = worker->cfg;
    int y;

    // Extract subgrids along second axis
    assert(subgrid_off0 % cfg->subgrid_spacing == 0);
    int subgrid_offset = subgrid_off0 / cfg->subgrid_spacing * cfg->yP_spacing;
    double start = get_time_ns();
    for (y = 0; y < cfg->xM_yN_size; y++) {
        extract_subgrid(cfg->yP_size, cfg->xM_yP_size, cfg->xMxN_yP_size, cfg->xM_yN_size,
                        subgrid_offset, cfg->m, cfg->Fn,
                        NMBF_BF+y*cfg->NMBF_BF_stride1, cfg->NMBF_BF_stride0,
                        worker->MBF, worker->MBF_plan,
                        NMBF_NMBF+y*cfg->NMBF_NMBF_stride1, cfg->NMBF_NMBF_stride0);
    }
    worker->es2_time += get_time_ns() - start;
    worker->es2_flops += cfg->xM_yN_size * worker->es_flops;

    // Check stream contents if requested
    int i0 = subgrid_off0 / worker->cfg->xA_size;
    int i1 = subgrid_off1 / worker->cfg->xA_size;
    if (cfg->stream_check) {

        // Check whether it exists
        char filename[256]; struct stat st;
        sprintf(filename, cfg->stream_check, i0, i1);
        if (stat(filename, &st) == 0) {

            double complex *NMBF_NMBF_check = read_dump(cfg->NMBF_NMBF_size, cfg->stream_check, i0, i1);
            if (NMBF_NMBF_check) {
                int x0; int errs = 0;
                for (x0 = 0; x0 < cfg->xM_yN_size * cfg->xM_yN_size; x0++) {
                    if (cabs(NMBF_NMBF[x0] - NMBF_NMBF_check[x0]) > cabs(NMBF_NMBF[x0]) * cfg->stream_check_threshold) {
                        fprintf(stderr, "stream check failed: subgrid %d/%d at position %d/%d (%f%+f != %f%+f)\n",
                                i0, i1, x0 / cfg->xM_yN_size, x0 % cfg->xM_yN_size,
                                creal(NMBF_NMBF[x0]), cimag(NMBF_NMBF[x0]),
                                creal(NMBF_NMBF_check[x0]), cimag(NMBF_NMBF_check[x0]));
                        errs+=1;
                    }
                }
                if (!errs) {
                    printf("stream check for subgrid %d/%d passed\n", i0, i1);
                }
            }
        }
    }

    // Similarly, write dump on request
    if (cfg->stream_dump) {
        write_dump(NMBF_NMBF, cfg->NMBF_NMBF_size, cfg->stream_dump, i0, i1);
    }

}

void recombine2d_af0_af1(struct recombine2d_config *cfg,
                         double complex *subgrid,
                         int facet_off0, int facet_off1,
                         double complex *NMBF_NMBF)
{
    assert(facet_off0 % cfg->facet_spacing == 0);
    assert(facet_off1 % cfg->facet_spacing == 0);

    // Calculate offsets to left-top corner, modulo size of subgrid / nmbf term
    int xM_size = cfg->xM_size, xM_yN_size = cfg->xM_yN_size;
    int facet_offset0 = facet_off0 / cfg->facet_spacing * cfg->xM_spacing;
    int facet_offset1 = facet_off1 / cfg->facet_spacing * cfg->xM_spacing;
    facet_offset0 += 2*xM_size - xM_yN_size/2; assert(facet_offset0 >= 0);
    facet_offset1 += 2*xM_size - xM_yN_size/2; assert(facet_offset1 >= 0);
    int nmbf_offset = (xM_yN_size+1)/2; // xM_yN_size - xM_yN_size/2

    int j0, j1;
    for (j0 = 0; j0 < xM_yN_size; j0++) {

        int off_sg0 = ((facet_offset0 + j0) % xM_size);
        int off_nmbf0 = ((nmbf_offset + j0) % xM_yN_size);

        for (j1 = 0; j1 < xM_yN_size; j1++) {

            int off_sg1 = ((facet_offset1 + j1) % xM_size);
            int off_nmbf1 = ((nmbf_offset + j1) % xM_yN_size);

            subgrid[off_sg0*xM_size+off_sg1] +=
                NMBF_NMBF[off_nmbf0*xM_yN_size+off_nmbf1] / (xM_size * xM_size);
        }
    }
}
