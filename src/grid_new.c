
#include "grid.h"

#include <assert.h>
#include <float.h>
#include <complex.h>
#include <fenv.h>
#ifdef __SSE4_1__
#include <immintrin.h>
#endif


__attribute__((always_inline))
inline static void _frac_coord(int oversample, double u, int *x, int *fx)
{

    assert(u >= 0);

    // Round to nearest oversampling value. We assume the kernel to be
    // in "natural" order, so what we are looking for is the best
    //    "x - fx / oversample"
    // approximation for u. Note that taken together we need about 35
    // significant bits here to deal with fine oversampling and large
    // grids. Really need double precision!
    double o = u * oversample;
#ifdef __SSE4_1__
    __m128d oxd = _mm_round_pd(_mm_set_pd(0, o),_MM_FROUND_TO_NEAREST_INT);
    int ox = oxd[0];
#else
    fesetround(3); int ox = lrint(o);
#endif
    ox += oversample - 1;
    *x = ox / oversample;
    *fx = oversample - 1 - (ox % oversample);

}

#ifdef __SSE4_1__
__attribute__((always_inline))
inline static void _frac_coord_uv(int oversample,
                                  double u, double v, int *px, int *pfx)
{

    // Round to nearest oversampling value. We assume the kernel to be
    // in "natural" order, so what we are looking for is the best
    //    "x - fx / oversample"
    // approximation for "grid_size/2 + u".
    __m128d ov = _mm_set_pd(oversample, oversample);
    __m128d o = _mm_set_pd(v, u);
    __m128d ox = _mm_round_pd(o * ov, _MM_FROUND_TO_NEAREST_INT | _MM_FROUND_NO_EXC);
    __m128d x = _mm_round_pd(ox / ov, _MM_FROUND_TO_POS_INF | _MM_FROUND_NO_EXC);
    __m128i ix = _mm_cvtpd_epi32(x);

    __attribute__((aligned(16))) int32_t ix_out[4];
    _mm_store_si128((__m128i *)ix_out, ix);
    px[0] = ix_out[0];
    px[1] = ix_out[1];

    __attribute__((aligned(16))) int32_t ifx_out[4];
    _mm_store_si128((__m128i *)ifx_out, _mm_cvtpd_epi32(ov * x - ox));
    pfx[0] = ifx_out[0];
    pfx[1] = ifx_out[1];

}
#endif

#ifdef __AVX__
__attribute__((always_inline))
inline static void _frac_coord_uvw(int oversample, int oversample_w,
                                   double u, double v, double w,
                                   int *px, int *pfx)
{

    // Round to nearest oversampling value. We assume the kernel to be
    // in "natural" order, so what we are looking for is the best
    //    "x - fx / oversample"
    // approximation for "grid_size/2 + u".
    __m256d ov = _mm256_set_pd(1, oversample_w, oversample, oversample);
    __m256d o = _mm256_set_pd(0, w, v, u);
    __m256d ox = _mm256_round_pd(o * ov, _MM_FROUND_TO_NEAREST_INT | _MM_FROUND_NO_EXC);
    __m256d x = _mm256_round_pd(ox / ov, _MM_FROUND_TO_POS_INF | _MM_FROUND_NO_EXC);
    
    __m128i ix = _mm256_cvtpd_epi32(x);
    __attribute__((aligned(16))) int32_t ix_out[4];
    _mm_store_si128((__m128i *)ix_out, ix);
    px[0] = ix_out[0];
    px[1] = ix_out[1];
    px[2] = ix_out[2];

    __m128i ifx = _mm256_cvtpd_epi32(ov * x - ox);
    __attribute__((aligned(16))) int32_t ifx_out[4];
    _mm_store_si128((__m128i *)ifx_out, ifx);
    pfx[0] = ifx_out[0];
    pfx[1] = ifx_out[1];
    pfx[2] = ifx_out[2];

}
#endif

/**
 * \brief Fractional coordinate separation from u,v,w to grid_offset, sub_offset_x, sub_offset_y, sub_offset_z.
 */
__attribute__((always_inline))
inline static void frac_coord_sep_uvw(int x_stride, int y_stride,
                                      int kernel_size, int kernel_stride, int over,
                                      int wkernel_size, int wkernel_stride, int over_w,
                                      double u, double v, double w,
                                      int *grid_offset, int *plane_offset,
                                      int *sub_offset_x, int *sub_offset_y, int *sub_offset_z)
{
    // Find fractional coordinates
    int ix[3], ixf[3];
#ifdef __AVX__
    _frac_coord_uvw(over, over_w, u, v, w, ix, ixf);
#else
#ifdef __SSE4_1__
    _frac_coord_uv(over, u, v, ix, ixf);
#else
    _frac_coord(over, u, &ix[0], &ixf[0]);
    _frac_coord(over, v, &ix[1], &ixf[1]);
#endif
    _frac_coord(over_w, w, &ix[2], &ixf[2]);
#endif
    
    // Calculate grid and oversampled kernel offsets
    *grid_offset = (ix[1]-kernel_size/2)*y_stride + (ix[0]-kernel_size/2)*x_stride;
    if (plane_offset) *plane_offset = ix[2]-wkernel_size/2;
    *sub_offset_x = kernel_stride * ixf[0];
    *sub_offset_y = kernel_stride * ixf[1];
    *sub_offset_z = wkernel_stride * ixf[2];
}

inline static
void degrid_line(int ch, int end_ch,
                 int w_size,
                 double im_mult,
                 double u, double v, double w,
                 double du_g, double dv_g, double dw_g,
                 double *kernel, int kernel_size, int kernel_oversampling, int kernel_stride, 
                 double *wkernel, int wkernel_size, int wkernel_oversampling, int wkernel_stride, 
                 double complex *pv, size_t vis_ch_stride,
                 double complex *grid, size_t grid_u_stride, size_t grid_v_stride, size_t grid_w_stride,
                 size_t grid_w_roll)
{

    for (; ch < end_ch; ch++, u+=du_g, v+=dv_g, w+=dw_g, pv+=vis_ch_stride) {

        // Calculate fractional coordinates
        int grid_offset, plane_offset, sub_offset_x, sub_offset_y, sub_offset_z;
        frac_coord_sep_uvw(grid_u_stride, grid_v_stride,
                           kernel_size, kernel_stride, kernel_oversampling,
                           wkernel_size, wkernel_stride, wkernel_oversampling,
                           u, v, w, &grid_offset, &plane_offset,
                           &sub_offset_x, &sub_offset_y, &sub_offset_z);

        // Get visibility by deconvolution along w,v,u axes
        double complex vis = 0;
        int z, y, x;
        for (z = 0; z < wkernel_size; z++) {
            double complex visz = 0;
            double complex *uvgrid = grid + ((z+plane_offset+grid_w_roll) % w_size) * grid_w_stride;
            for (y = 0; y < kernel_size; y++) {
                double complex visy = 0;
                for (x = 0; x < kernel_size; x++) {
                    visy += kernel[sub_offset_x + x] *
                        uvgrid[grid_offset + y*grid_v_stride + x*grid_u_stride];
                }
                visz += kernel[sub_offset_y + y] * visy;
            }
            vis += wkernel[sub_offset_z + z] * visz;
        }
        vis = creal(vis) + I * cimag(vis) * im_mult;
        *pv = vis;

    }
}
    
void degrid(int u0, size_t u_size,
            int v0, size_t v_size,
            int w0, size_t w_size,
            int step0, size_t step_count,
            int ch0, size_t ch_count,
            struct gridder_plan *plan,
            bool conjugate,
            double *uvw_ld, int *channels,
            double complex *grid, size_t grid_u_stride, size_t grid_v_stride, size_t grid_w_stride,
            size_t grid_w_roll,
            double complex *vis_out, size_t vis_ch_stride, size_t vis_step_stride)
{

    struct sep_kernel_data *kernel = &plan->kernel;
    struct sep_kernel_data *wkernel = &plan->wkernel;
    const size_t kernel_size = kernel->size;
    const size_t wkernel_size = wkernel->size;
    const double im_mult = (conjugate ? -1 : 1);
    const double grid_du = plan->du;
    const double grid_dv = plan->dv;
    const double grid_dw = plan->dw;

    // Go through steps
    int step;
    for (step = 0; step < step_count; step++) {

        // Get channels
        int start_ch = channels[2*step];
        int end_ch = channels[2*step+1];
        if (start_ch < ch0) start_ch = ch0;
        if (end_ch > ch0+ch_count) end_ch = ch0+ch_count;
        if (start_ch >= end_ch) start_ch = ch0+ch_count;

        // Fill start with zeroes
        int ch;
        double complex *pv = vis_out + step*vis_step_stride;
        for (ch = ch0; ch < start_ch; ch++, pv+=vis_ch_stride)
            *pv = 0;
        if (ch > end_ch)
            continue;

        // Calculate start and end of visibility line - and make
        // extra-sure it actually sits within our set bounds.
        double *uvw0 = uvw_ld + 6 * step;
        const double du = uvw0[3], dv = uvw0[4], dw = uvw0[5];
        double u = (uvw0[0]+start_ch*du)/grid_du - u0;
        double v = (uvw0[1]+start_ch*dv)/grid_dv - v0;
        double w = (uvw0[2]+start_ch*dw)/grid_dw - w0;
        int nch = end_ch - start_ch - 1;
        double u1 = u + du/grid_du * nch;
        double v1 = v + dv/grid_dv * nch;
        double w1 = w + dw/grid_dw * nch;
        assert(u - kernel_size / 2 > 0 && u + kernel_size / 2 < u_size);
        assert(u1 - kernel_size / 2  > 0 && u1 + kernel_size / 2 < u_size);
        assert(v - kernel_size / 2 > 0 && v + kernel_size / 2 < v_size+1);
        assert(v1 - kernel_size / 2 > 0 && v1 + kernel_size / 2 < v_size+1);
        assert(w > wkernel_size / 2 - 1 && w - 1e-3 < w_size - wkernel_size / 2);
        assert(w1 > wkernel_size / 2 - 1 && w1 - 1e-3 < w_size - wkernel_size / 2);

        // Check for specialised kernels (note that all the included
        // code looks like "if (...) { ... } else")
#ifdef __AVX2__
        #include "griduvw2_avx2_6_4.h"
        #include "griduvw2_avx2_6_6.h"
        #include "griduvw2_avx2_8_4.h"
        #include "griduvw2_avx2_8_6.h"
        #include "griduvw2_avx2_10_4.h"
        #include "griduvw2_avx2_10_6.h"
        #include "griduvw2_avx2_12_4.h"
        #include "griduvw2_avx2_12_6.h"
#endif
        {
        // Degrid a line of visibilities
        degrid_line(ch, end_ch, w_size, im_mult,
                    u, v, w, du/grid_du, dv/grid_dv, dw/grid_dw,
                    kernel->data, kernel->size, kernel->oversampling, kernel->stride,
                    wkernel->data, wkernel->size, wkernel->oversampling, wkernel->stride,
                    pv, vis_ch_stride, grid, grid_u_stride, grid_v_stride, grid_w_stride, grid_w_roll);
        }

        // Fill rest with zeroes
        for (ch = end_ch; ch < ch0+ch_count; ch++)
            vis_out[step*vis_step_stride + (ch-ch0)*vis_ch_stride] = 0;

    }

}
