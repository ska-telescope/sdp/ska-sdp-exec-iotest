#include "grid.h"

#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <assert.h>
#include <float.h>
#include <fcntl.h>
#include <stdint.h>
#include <complex.h>
#include <fftw3.h>
#include <omp.h>
#include <string.h>
#include <fenv.h>
#ifdef __SSE4_1__
#include <immintrin.h>
#endif

// Assume all uvw are zero. This eliminates all coordinate
// calculations and grids only into the middle.
//#define ASSUME_UVW_0

inline static void _frac_coord(int oversample, double u, int *x, int *fx)
{

    assert(u >= 0);

    // Round to nearest oversampling value. We assume the kernel to be
    // in "natural" order, so what we are looking for is the best
    //    "x - fx / oversample"
    // approximation for u. Note that taken together we need about 35
    // significant bits here to deal with fine oversampling and large
    // grids. Really need double precision!
    double o = u * oversample;
#ifdef __SSE4_1__
    __m128d oxd = _mm_round_pd(_mm_set_pd(0, o),_MM_FROUND_TO_NEAREST_INT);
    int ox = oxd[0];
#else
    fesetround(3); int ox = lrint(o);
#endif
    ox += oversample - 1;
    *x = ox / oversample;
    *fx = oversample - 1 - (ox % oversample);

}

void frac_coord(int grid_size, int oversample,
                double u, int *x, int *fx)
{

    _frac_coord(oversample, grid_size/2 + u, x, fx);

}

#ifdef __SSE4_1__
inline static void _frac_coord_uv(int oversample,
                                  double u, double v, int *px, int *pfx)
{

    // Round to nearest oversampling value. We assume the kernel to be
    // in "natural" order, so what we are looking for is the best
    //    "x - fx / oversample"
    // approximation for "grid_size/2 + u".
    __m128d ov = _mm_set_pd(oversample, oversample);
    __m128d o = _mm_set_pd(v, u);
    __m128d ox = _mm_round_pd(o * ov, _MM_FROUND_TO_NEAREST_INT | _MM_FROUND_NO_EXC);
    __m128d x = _mm_round_pd(ox / ov, _MM_FROUND_TO_POS_INF | _MM_FROUND_NO_EXC);
    __m128i ix = _mm_cvtpd_epi32(x);

    __attribute__((aligned(16))) int32_t ix_out[4];
    _mm_store_si128((__m128i *)ix_out, ix);
    px[0] = ix_out[0];
    px[1] = ix_out[1];

    __attribute__((aligned(16))) int32_t ifx_out[4];
    _mm_store_si128((__m128i *)ifx_out, _mm_cvtpd_epi32(ov * x - ox));
    pfx[0] = ifx_out[0];
    pfx[1] = ifx_out[1];

}

void frac_coord_uv(int grid_size, int oversample,
                   double u, double v, int *x, int *fx)
{

    _frac_coord_uv(oversample, grid_size/2+u, grid_size/2+v, x, fx);

}
#endif

/**
 * \brief Fractional coordinate calculation for separable 1D kernel.
 * Results are stored in grid_offset, sub_offset_x, sub_offset_y.
 */
inline static void frac_coord_sep_uv(int grid_size, int grid_stride,
                                     int kernel_size, int kernel_stride, int oversample,
                                     double theta,
                                     double u, double v,
                                     int *grid_offset,
                                     int *sub_offset_x, int *sub_offset_y)
{

    double x = theta * u, y = theta * v;
    // Find fractional coordinates
    int ix, iy, ixf, iyf;
    _frac_coord(oversample, grid_size/2 + x, &ix, &ixf);
    _frac_coord(oversample, grid_size/2 + y, &iy, &iyf);
    // Calculate grid and oversampled kernel offsets
    *grid_offset = (iy-kernel_size/2)*grid_stride + (ix-kernel_size/2);
    *sub_offset_x = kernel_stride * ixf;
    *sub_offset_y = kernel_stride * iyf;
}

/**
 * \brief Fractional coordinate separation from u,v,w to grid_offset, sub_offset_x, sub_offset_y, sub_offset_z.
 */
inline static void frac_coord_sep_uvw(int x_stride, int y_stride,
                                      int kernel_size, int kernel_stride, int over,
                                      int wkernel_size, int wkernel_stride, int over_w,
                                      double u, double v, double w,
                                      int *grid_offset, int *plane_offset,
                                      int *sub_offset_x, int *sub_offset_y, int *sub_offset_z)
{
    // Find fractional coordinates
    int ix[2], iz, ixf[2], izf;
#ifndef __SSE4_1__
    _frac_coord(over, u, &ix[0], &ixf[0]);
    _frac_coord(over, v, &ix[1], &ixf[1]);
#else
    _frac_coord_uv(over, u, v, ix, ixf);
#endif
    _frac_coord(over_w, w, &iz, &izf);

    // Calculate grid and oversampled kernel offsets
    *grid_offset = (ix[1]-kernel_size/2)*y_stride + (ix[0]-kernel_size/2)*x_stride;
    if (plane_offset) *plane_offset = iz-wkernel_size/2;
    *sub_offset_x = kernel_stride * ixf[0];
    *sub_offset_y = kernel_stride * ixf[1];
    *sub_offset_z = wkernel_stride * izf;
}

inline static
void degrid_conv_uv(double complex *uvgrid, int grid_size, int grid_stride, double theta,
                    double complex mult,
                    double u, double v,
                    struct sep_kernel_data *kernel,
                    uint64_t *flops, double complex *pvis)
{

    // Calculate grid and sub-grid coordinates
    int grid_offset, sub_offset_x, sub_offset_y;
    const int kernel_size = kernel->size;
    frac_coord_sep_uv(grid_size, grid_stride, kernel_size, kernel->stride, kernel->oversampling,
                      theta, u, v,
                      &grid_offset, &sub_offset_x, &sub_offset_y);

#ifndef __AVX2__
    // Get visibility
    double complex vis = 0;
    int y, x;
    for (y = 0; y < kernel_size; y++) {
        double complex visy = 0;
        for (x = 0; x < kernel_size; x++) {
            visy += kernel->data[sub_offset_x + x] *
                    uvgrid[grid_offset + y*grid_stride + x];
        }
        vis += kernel->data[sub_offset_y + y] * visy;
    }
    *flops += 4 * (1 + kernel_size) * kernel_size;
    *pvis = creal(vis) * creal(mult) + I * cimag(vis) * cimag(mult);
#else

    double complex *pgrid = uvgrid + grid_offset;
    double *kernel_x = kernel->data + sub_offset_x;
    double *kernel_y = kernel->data + sub_offset_y;

    // Get visibility
    assert(kernel_size % 2 == 0);
    __m256d vis = _mm256_setzero_pd();
    int y, x;
    for (y = 0; y < kernel_size; y += 1) {
        __m256d grid = _mm256_loadu_pd((double *)(pgrid + y*grid_stride));
        double *pk = kernel_x;
        __m256d kern = _mm256_setr_pd(*pk, *pk, *(pk+1), *(pk+1));
        __m256d sum = _mm256_mul_pd(kern, grid);
        _mm_prefetch(uvgrid + grid_offset + (y+1)*grid_stride, _MM_HINT_T0);
        for (x = 2, pk += 2; x < kernel_size; x += 2, pk += 2) {
            __m256d kern = _mm256_setr_pd(*pk, *pk, *(pk+1), *(pk+1));
            __m256d grid = _mm256_loadu_pd((double *)(pgrid + y*grid_stride + x));
            sum = _mm256_fmadd_pd(kern, grid, sum);
        }
        double kern_y = kernel_y[y];
        vis = _mm256_fmadd_pd(sum, _mm256_set1_pd(kern_y), vis);
    }

    __m128d vis_out = _mm256_extractf128_pd(vis, 0) + _mm256_extractf128_pd(vis, 1);
    _mm_store_pd((double *)pvis, vis_out * _mm_load_pd((double *)&mult));

    *flops += 4 * (1 + kernel_size) * kernel_size;
#endif
}

inline static int imax(int a, int b) { return a >= b ? a : b; }
inline static int imin(int a, int b) { return a <= b ? a : b; }

void degrid_conv_uv_line(double complex *uvgrid, int grid_size, int grid_stride,
                         double theta, double u0, double v0, double w0,
                         double du, double dv, double dw, int count,
                         double min_u, double max_u,
                         double min_v, double max_v,
                         double min_w, double max_w,
                         bool conjugate,
                         struct sep_kernel_data *kernel,
                         double complex *pvis0, uint64_t *flops, uint64_t *vis_count)
{

    // Figure out bounds
    int i0 = 0, i1 = count;
    if (du > 0) {
        if (u0+i0*du < min_u) i0 = imax(i0, ceil( (min_u - u0) / du ));
        if (u0+i1*du > max_u) i1 = imin(i1, ceil( (max_u - u0) / du ));
    } else if (du < 0) {
        if (u0+i0*du > max_u) i0 = imax(i0, ceil( (max_u - u0) / du ));
        if (u0+i1*du < min_u) i1 = imin(i1, ceil( (min_u - u0) / du ));
    }
    if (dv > 0) {
        if (v0+i0*dv < min_v) i0 = imax(i0, ceil( (min_v - v0) / dv ));
        if (v0+i1*dv > max_v) i1 = imin(i1, ceil( (max_v - v0) / dv ));
    } else if (dv < 0) {
        if (v0+i0*dv > max_v) i0 = imax(i0, ceil( (max_v - v0) / dv ));
        if (v0+i1*dv < min_v) i1 = imin(i1, ceil( (min_v - v0) / dv ));
    }
    if (dw > 0) {
        if (w0+i0*dw < min_w) i0 = imax(i0, ceil( (min_w - w0) / dw ));
        if (w0+i1*dw > max_w) i1 = imin(i1, ceil( (max_w - w0) / dw ));
    } else if (dw < 0) {
        if (w0+i0*dw > max_w) i0 = imax(i0, ceil( (max_w - w0) / dw ));
        if (w0+i1*dw < min_w) i1 = imin(i1, ceil( (min_w - w0) / dw ));
    }
    i0 = imax(0, imin(i0, i1));

    // Count visibilities degridded
    if (vis_count && i1 > i0) *vis_count += (i1 - i0);

    /* printf("Skip %d / degrid %d / skip %d\n", i0, i1-i0, count-i1); */

    // Fill zeroes
    int i = 0; double complex *pvis = pvis0;
    double u = u0, v = v0, w = w0;
    for (; i < i0; i++, u += du, v += dv, w += dw, pvis++) {
        assert(!( u >= min_u && u < max_u &&
                  v >= min_v && v < max_v &&
                  w >= min_w && w < max_w ));
        *pvis = 0.;
    }

    // Anything to do?
    if (i < i1) {

#ifdef __AVX2__
        if (kernel->size == 8) {
            #include "grid_avx2_8.h"
        } else if (kernel->size == 10) {
            #include "grid_avx2_10.h"
        } else if (kernel->size == 12) {
            #include "grid_avx2_12.h"
        } else if (kernel->size == 14) {
            #include "grid_avx2_14.h"
        } else if (kernel->size == 16) {
            #include "grid_avx2_16.h"

        } else
#endif
        {

            double complex mult = (conjugate ? 1 - I : 1 + I);
            for (; i < i1; i++, u += du, v += dv, w += dw, pvis+=1) {
                if (!(u >= min_u && u < max_u &&
                      v >= min_v && v < max_v &&
                      w >= min_w && w < max_w)){
                    printf("%g %g %g  %g %g %g  %g %g %g\n",
                           u, min_u, max_u,
                           v, min_v, max_v,
                           w, min_w, max_w);
                    printf("%g %g %g\n", du, dv, dw);
                    assert(false);
                }
                assert(u >= min_u && u < max_u &&
                       v >= min_v && v < max_v &&
                       w >= min_w && w < max_w);
                degrid_conv_uv(uvgrid, grid_size, grid_stride, theta,
                               mult, u, v, kernel, flops, pvis);
            }

        }
    }

    // Fill remaining zeroes
    for (; i < count; i++, u += du, v += dv, w += dw, pvis++) {
        assert(!( u >= min_u && u < max_u &&
                  v >= min_v && v < max_v &&
                  w >= min_w && w < max_w ));
        *pvis = 0.;
    }

}

inline static
void degrid_conv_uvw(double complex **uvgrids, int x_stride, int y_stride,
                     double complex mult, double u, double v, double w,
                     struct sep_kernel_data *kernel, struct sep_kernel_data *wkernel,
                     uint64_t *flops, double complex *pvis)
{

    const int kernel_size = kernel->size, wkernel_size = wkernel->size;
    int grid_offset, sub_offset_x, sub_offset_y, sub_offset_z;
    frac_coord_sep_uvw(x_stride, y_stride,
                       kernel_size, kernel->stride, kernel->oversampling,
                       wkernel_size, wkernel->stride, wkernel->oversampling,
                       u, v, w, &grid_offset, NULL, &sub_offset_x, &sub_offset_y, &sub_offset_z);

    /* printf("(old) %g %g %g -> offsets: %d  %d %d %d\n", */
    /*        u, v, w, grid_offset, sub_offset_x, sub_offset_y, sub_offset_z); */

    // Get visibility
    double complex vis = 0;
    int z, y, x;
    for (z = 0; z < wkernel_size; z++) {
        double complex visz = 0;
        double complex *uvgrid = uvgrids[z];
        for (y = 0; y < kernel_size; y++) {
            double complex visy = 0;
            for (x = 0; x < kernel_size; x++) {
                visy += kernel->data[sub_offset_x + x] *
                    uvgrid[grid_offset + y*y_stride + x*x_stride];
            }
            visz += kernel->data[sub_offset_y + y] * visy;
        }
        vis += wkernel->data[sub_offset_z + z] * visz;
    }
    *flops += 4 * (1 + kernel_size) * kernel_size * wkernel_size;
    //printf("-> %g%+gj\n", creal(vis), cimag(vis));
    *pvis = creal(vis) * creal(mult) + I * cimag(vis) * cimag(mult);
}

void degrid_conv_uvw_line(double complex **uvgrids, int grid_size, int x_stride, int y_stride,
                          double theta, double wstep, double u0, double v0, double w0,
                          double du, double dv, double dw, int count,
                          double min_u, double max_u,
                          double min_v, double max_v,
                          double min_w, double max_w,
                          bool conjugate,
                          struct sep_kernel_data *kernel, struct sep_kernel_data *wkernel,
                          double complex *pvis0, uint64_t *flops, uint64_t *vis_count)
{

    // Figure out bounds
    int i0 = 0, i1 = count;
    if (du > 0) {
        if (u0+i0*du < min_u) i0 = imax(i0, ceil( (min_u - u0) / du ));
        if (u0+i1*du > max_u) i1 = imin(i1, ceil( (max_u - u0) / du ));
    } else if (du < 0) {
        if (u0+i0*du > max_u) i0 = imax(i0, ceil( (max_u - u0) / du ));
        if (u0+i1*du < min_u) i1 = imin(i1, ceil( (min_u - u0) / du ));
    }
    if (dv > 0) {
        if (v0+i0*dv < min_v) i0 = imax(i0, ceil( (min_v - v0) / dv ));
        if (v0+i1*dv > max_v) i1 = imin(i1, ceil( (max_v - v0) / dv ));
    } else if (dv < 0) {
        if (v0+i0*dv > max_v) i0 = imax(i0, ceil( (max_v - v0) / dv ));
        if (v0+i1*dv < min_v) i1 = imin(i1, ceil( (min_v - v0) / dv ));
    }
    if (dw > 0) {
        if (w0+i0*dw < min_w) i0 = imax(i0, fmin(i1, ceil( (min_w - w0) / dw )));
        if (w0+i1*dw > max_w) i1 = imin(i1, fmax(i0, ceil( (max_w - w0) / dw )));
    } else if (dw < 0) {
        if (w0+i0*dw > max_w) i0 = imax(i0, fmin(i1, ceil( (max_w - w0) / dw )));
        if (w0+i1*dw < min_w) i1 = imin(i1, fmax(i0, ceil( (min_w - w0) / dw )));
    }
    i0 = imax(0, imin(i0, i1));
    //printf("(old) i0=%d i1=%d\n", i0, i1);

    // Count visibilities degridded
    if (vis_count && i1 > i0) *vis_count += (i1 - i0);

    // Fill zeroes
    int i = 0; double complex *pvis = pvis0;
    for (; i < i0; i++, pvis++) {
        *pvis = 0.;
    }

    // Anything to do?
    if (i < i1) {

        // Calculate the start coordinate directly - this prevents
        // some rare rounding problems where w+dw+..+dw != w+i0*dw
        double u = u0 + i * du;
        double v = v0 + i * dv;
        double w = w0 + i * dw;
        if ( i > 0 &&
             u-du >= min_u && u-du < max_u &&
             v-dv >= min_v && v-dv < max_v &&
             w-dw >= min_w && w-dw < max_w) {

            printf("%d/%d  %g %g %g  %g %g %g  %g %g %g\n", i, count,
                   min_u, u-du, max_u,  min_v, v-dv, max_v,  min_w, w-dw, max_w);

            printf("%g+i*%g  %g+i*%g  %g+i*%g\n", u0, du, v0, dv, w0, dw);
            if (dw > 0) {
                printf("i0 = %d = imax(i0, fmin(i1, %g))\n", i0, (min_w - w0) / dw);
                printf("i1 = %d = imin(i1, fmax(i0, %g))\n", i1, (max_w - w0) / dw);
            } else {
                printf("i0 = %d = imax(i0, fmin(i1, %g))\n", i0, (max_w - w0) / dw);
                printf("i1 = %d = imin(i1, fmax(i0, %g))\n", i1, (min_w - w0) / dw);
            }
            fflush(stdout);
            fflush(stdout);
        }

        assert(!( i> 0 && u-du >= min_u && u-du < max_u &&
                  v-dv >= min_v && v-dv < max_v &&
                  w-dw >= min_w && w-dw < max_w) );
        assert(u >= min_u && u < max_u &&
               v >= min_v && v < max_v &&
               w >= min_w && w < max_w);

#ifdef __AVX2__
        if (kernel->size == 8 && wkernel->size == 4 && x_stride==1) {
            #include "griduvw_avx2_8_4.h"
        } else if (kernel->size == 8 && wkernel->size == 4 && x_stride == 4) {
            // Determine phase - not elegant, needs to be improved...
            if (uvgrids[3] > uvgrids[0]) {
                #include "griduvw_avx2_8_4+0.h"
            } else if (uvgrids[0] > uvgrids[1]) {
                #include "griduvw_avx2_8_4+1.h"
            } else if (uvgrids[1] > uvgrids[2]) {
                #include "griduvw_avx2_8_4+2.h"
            } else {
                #include "griduvw_avx2_8_4+3.h"
            }
        } else if (kernel->size == 8 && wkernel->size == 6 && x_stride==1) {
            #include "griduvw_avx2_8_6.h"
        } else
#endif
        {
            double complex mult = (conjugate ? 1 - I : 1 + I);
            for (; i < i1; i++, u += du, v += dv, w += dw, pvis+=1) {
                assert(u >= min_u && u < max_u &&
                       v >= min_v && v < max_v &&
                       w >= min_w && w < max_w);
                degrid_conv_uvw(uvgrids, x_stride, y_stride, mult,
                                u,v,w, kernel, wkernel, flops, pvis);
            }
        }
    }

    // Fill remaining zeroes
    for (; i < count; i++, pvis++) {
        *pvis = 0.;
    }

}

int clamp_channels(size_t step0, size_t step_count,
                   size_t ch0, size_t ch_count,
                   double min_u, double max_u,
                   double min_v, double max_v,
                   double min_w, double max_w,
                   double *uvw_ld, int *channels_out)
{

    int total_visibilities = 0;
    int step;
    for (step = 0; step < step_count; step++) {
        double *uvw0 = uvw_ld + step * 6;
        double u0 = uvw0[0], v0 = uvw0[1], w0 = uvw0[2];
        double du = uvw0[3], dv = uvw0[4], dw = uvw0[5];

        // Figure out bounds
        int i0 = ch0, i1 = ch0 + ch_count;
        if (du > 0) {
            if (u0+i0*du < min_u) i0 = imax(i0, ceil( (min_u - u0) / du ));
            if (u0+i1*du > max_u) i1 = imin(i1, ceil( (max_u - u0) / du ));
        } else if (du < 0) {
            if (u0+i0*du > max_u) i0 = imax(i0, ceil( (max_u - u0) / du ));
            if (u0+i1*du < min_u) i1 = imin(i1, ceil( (min_u - u0) / du ));
        }
        if (dv > 0) {
            if (v0+i0*dv < min_v) i0 = imax(i0, ceil( (min_v - v0) / dv ));
            if (v0+i1*dv > max_v) i1 = imin(i1, ceil( (max_v - v0) / dv ));
        } else if (dv < 0) {
            if (v0+i0*dv > max_v) i0 = imax(i0, ceil( (max_v - v0) / dv ));
            if (v0+i1*dv < min_v) i1 = imin(i1, ceil( (min_v - v0) / dv ));
        }
        if (dw > 0) {
            if (w0+i0*dw < min_w) i0 = imax(i0, fmin(i1, ceil( (min_w - w0) / dw )));
            if (w0+i1*dw > max_w) i1 = imin(i1, fmax(i0, ceil( (max_w - w0) / dw )));
        } else if (dw < 0) {
            if (w0+i0*dw > max_w) i0 = imax(i0, fmin(i1, ceil( (max_w - w0) / dw )));
            if (w0+i1*dw < min_w) i1 = imin(i1, fmax(i0, ceil( (min_w - w0) / dw )));
        }

        // Set clamped channel numbers
        i0 = imax(ch0, imin(i0, i1));
        if (i0 >= i1) {
            channels_out[step*2+0] = 0;
            channels_out[step*2+1] = 0;
        } else {
            channels_out[step*2+0] = i0;
            channels_out[step*2+1] = i1;
            // Count total unclamped visibilities
            total_visibilities += (i1 - i0);
        }
    }

    return total_visibilities;
}

uint64_t degrid_conv_bl(double complex *uvgrid, int grid_size, int grid_stride, double theta,
                        double d_u, double d_v,
                        double min_u, double max_u, double min_v, double max_v,
                        struct bl_data *bl,
                        int time0, int time1, int freq0, int freq1,
                        struct sep_kernel_data *kernel)
{
    uint64_t flops = 0;
    int time, freq;
    for (time = time0; time < time1; time++) {
        for (freq = freq0; freq < freq1; freq++) {

            // Bounds check
            double u = uvw_lambda(bl, time, freq, 0);
            if (u < min_u || u >= max_u) continue;
            double v = uvw_lambda(bl, time, freq, 1);
            if (v < min_v || v >= max_v) continue;

            degrid_conv_uv(uvgrid, grid_size, grid_stride, theta,
                           1+I,
                           u-d_u, v-d_v, kernel, &flops,
                           bl->vis + (time-time0)*(freq1 - freq0) + freq-freq0);
        }
    }

    return flops;
}

/**
 * \brief Shift the FFT.
 */
void fft_shift(double complex *uvgrid, int grid_size, int grid_stride)
{
    assert(grid_size % 2 == 0);
    int x, y;
    for (y = 0; y < grid_size; y++) {
        for (x = 0; x < grid_size/2; x++) {
            int ix0 = y * grid_stride + x;
            int ix1 = (ix0 + (grid_stride+1) * (grid_size/2)) % (grid_stride*grid_size);
            double complex temp = uvgrid[ix0];
            uvgrid[ix0] = uvgrid[ix1];
            uvgrid[ix1] = temp;
        }
    }
}
