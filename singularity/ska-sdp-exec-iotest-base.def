Bootstrap: docker
From: debian:buster-slim

%post
    . /.singularity.d/env/10-docker*.sh

%setup
    # Get the OpenMPI version on the host and save to a file in container root file system to access it later
    if [ ! -z "$(whereis mpicc | awk '{print $2}')" ]
    then
        # OMPI_VERSION_INFO=$(ompi_info --version)
        # [[ $OMPI_VERSION_INFO =~ ^.*v([0-9]*)\.([0-9]*)\.([0-9]*).* ]]
        # echo ${BASH_REMATCH[1]}.${BASH_REMATCH[2]}.${BASH_REMATCH[3]} > ${SINGULARITY_ROOTFS}/ompi_host_version
        # For some reason the above snippet doesnt work on Grid'5000. However, it works on p3-openhpc and possibly other machines without issues.
        ompi_info --version | grep 'Open MPI' | sed  's/^.*v\([0-9]*.[0-9]*.[0-9]*\).*$/\1/g' > ${SINGULARITY_ROOTFS}/ompi_host_version
        # echo "4.0.1" > ${SINGULARITY_ROOTFS}/ompi_host_version
    else
        echo "No MPI Installation found. Please load appropriate modules or build the image file on a machine with OpenMPI installation."
        exit 1
    fi

%post
    export OMPI3_DIR=/usr/local/ompi-3.1-ibverbs
    export OMPI4_DIR=/usr/local/ompi-4.1-ucx

%post
    set -e
    echo "Installing essential packages..."
    apt-get update && apt-get install -y build-essential \
                                         wget \
                                         curl \
                                         git \
                                         bash \
                                         file \
                                         bzip2
    apt-get clean

%post
    set -e
    echo "Installing compiler toolchains..."
    apt-get update && apt-get install -y gfortran \
                                         gcc \
                                         g++ \
                                         cmake
    apt-get clean

%post
    set -e
    echo "Installing OEFD packages..."
    apt-get update && apt-get install -y libibverbs-dev \
                                         rdma-core \
                                         librdmacm-dev \
                                         libnuma-dev
    apt-get clean

%post
    echo "Installing Open MPI 3.1.3 with ibverbs support"
    OMPI_VERSION='3.1.3'
    OMPI_VERSION_MAJOR=$(echo $OMPI_VERSION | awk -F "." '{print $1}')
    OMPI_VERSION_MAJOR_MINOR=$(echo $OMPI_VERSION | awk -F "." '{print $1, $2}' | tr ' ' '.')
    OMPI_URL="https://download.open-mpi.org/release/open-mpi/v$OMPI_VERSION_MAJOR_MINOR/openmpi-$OMPI_VERSION.tar.bz2"
    mkdir -p /tmp/ompi-3.1
    # Download
    cd /tmp/ompi-3.1 && wget -O openmpi-$OMPI_VERSION.tar.bz2 $OMPI_URL && tar -xjf openmpi-$OMPI_VERSION.tar.bz2

    # Compile and install
    config_opts="--prefix=$OMPI3_DIR --enable-orterun-prefix-by-default --enable-static --enable-shared --with-verbs"
    cd /tmp/ompi-3.1/openmpi-$OMPI_VERSION && ./configure ${config_opts}
    make -j$(nproc)
    make -j$(nproc) install
    rm -rf /tmp/ompi-3.1

%post
    echo "Installing Open MPI 4.1.0 with ucx support"
    # Install UCX
    # Starting from OpenMPI 4.0, UCX PML is preferred method for IB support
    # (https://www.open-mpi.org/software/ompi/major-changes.php)
    mkdir -p /tmp/ucx
    cd /tmp/ucx && wget https://github.com/openucx/ucx/releases/download/v1.10.0/ucx-1.10.0.tar.gz
    tar xzf ucx-1.10.0.tar.gz
    cd ucx-1.10.0
    ucx_config_opts="--prefix=$OMPI4_DIR/ucx --enable-optimizations --enable-mt --enable-cma --with-rc --with-ud --with-dc --with-rdmacm --with-mlx5-dv --with-ib-hw-tm --with-dm"
    ./contrib/configure-release ${ucx_config_opts}
    make -j$(nproc) install

    OMPI_VERSION='4.1.0'
    OMPI_VERSION_MAJOR=$(echo $OMPI_VERSION | awk -F "." '{print $1}')
    OMPI_VERSION_MAJOR_MINOR=$(echo $OMPI_VERSION | awk -F "." '{print $1, $2}' | tr ' ' '.')
    OMPI_URL="https://download.open-mpi.org/release/open-mpi/v$OMPI_VERSION_MAJOR_MINOR/openmpi-$OMPI_VERSION.tar.bz2"
    mkdir -p /tmp/ompi-4.1
    # Download
    cd /tmp/ompi-4.1 && wget -O openmpi-$OMPI_VERSION.tar.bz2 $OMPI_URL && tar -xjf openmpi-$OMPI_VERSION.tar.bz2

    # Compile and install
    config_opts="--prefix=$OMPI4_DIR --enable-orterun-prefix-by-default --enable-static --enable-shared --with-ucx=$OMPI4_DIR/ucx --without-verbs --enable-mca-no-build=btl-uct"
    cd /tmp/ompi-4.1/openmpi-$OMPI_VERSION && ./configure ${config_opts}
    make -j$(nproc)
    make -j$(nproc) install
    rm -rf /tmp/ucx
    rm -rf /tmp/ompi-4.1

%post

    echo "Get and install git lfs"
    mkdir -p /tmp/git-lfs && cd /tmp/git-lfs
    wget https://github.com/git-lfs/git-lfs/releases/download/v2.10.0/git-lfs-linux-amd64-v2.10.0.tar.gz
    tar -xf git-lfs-linux-amd64-v2.10.0.tar.gz
    rm git-lfs-linux-amd64-v2.10.0.tar.gz
    cp git-lfs /usr/local/bin/
    git lfs install
    rm -rf /tmp/git-lfs

    echo "Get and install cmake"
    mkdir -p /tmp/cmake
    cd /tmp/cmake
    wget https://github.com/Kitware/CMake/releases/download/v3.20.0/cmake-3.20.0-linux-x86_64.tar.gz
    tar -xf cmake-3.20.0-linux-x86_64.tar.gz
    rm cmake-3.20.0-linux-x86_64.tar.gz
    cp -r cmake-3.20.0-linux-x86_64/bin /usr/local
    cp -r cmake-3.20.0-linux-x86_64/share/cmake-3.20 /usr/local/share
    rm -rf /tmp/cmake

%help

This singularity recipe file builds a base image that will be used to build the Imaging IO test image file. This base image compiles OpenMPI 3.1.3 and OpenMPI 4.1.0. In OpenMPI 3.1.3, the preferred way of IB support is using IB verbs whereas in OpenMPI 4.1.0 it is by ucx. So OpenMPI 3.1.3 is compiled with full ibverbs support and OpenMPI 4.1.0 is compiled with ucx support. We are using two different OpenMPI here to improve the protability of the image file.

The idea is to load this base image when building the final Imaging IO test image and use the pre-compiled OpenMPI binaries to build different apps.

%labels
    Author Team_PLANET
    email mahendra.paipuri@inria.fr
    ImageName Imaging IO Test Base Container
