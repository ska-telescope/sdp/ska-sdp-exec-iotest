Known Issues
============

As already discussed throughout the documentation, there are some known
issues in the benchmark. The important ones are

-  It is imperative to use ``--fork-writer`` option to achieve high
   data throughput with multiple writers. This limitation comes from
   the global lock of the HDF5 libraries.
-  The option ``--fork-writer`` does not work on Mac as
   ``dispatch_semaphore`` library used for the implementing locks on
   writers supports only threads. Typically, the writer processes
   hangs waiting for the lock to release. One possible solution to
   this is to use named semaphores from POSIX (MacOS supports only
   named Semaphores). But using named Semaphores gives us another
   limitation as typically the number of opened file descriptors are
   limited. To get around this issue we can increase the ``ulimit -n``
   (till the value the kernel accepts) and/or decrease visibility
   queue size for the benchmark ``--visibility-queue``.
-  Often when running on MacOS, OpenMPI that is build with
   ``homebrew`` might throw some silent errors concerning vader BTL
   component. They might not be critical but to get rid of them
   include only ``tcp`` and ``self`` BTL components using
   ``--mca btl tcp,self`` argument.
