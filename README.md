
SDP Distributed Predict/Imaging I/O Prototype
=============================================

This is a prototype exploring the capability of hardware and software to deal with the types of I/O loads that the SDP will have to support for full-scale operation on SKA1 (and beyond). The benchmark is written in plain C and uses MPI for communication. The source code is directly accessible from these documentation pages: see the source link in the top right corner.

[![Documentation Status](https://readthedocs.org/projects/ska-telescope-ska-sdp-exec-iotest/badge/?version=latest)](https://developer.skao.int/projects/ska-sdp-exec-iotest/en/latest/?badge=latest)

The [Documentation](https://developer.skao.int/projects/ska-sdp-exec-iotest/en/latest/?badge=latest) includes context, algorithmic design aspects, usage examples and installation directions.
